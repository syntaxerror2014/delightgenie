﻿using DelightGenie.DAL;
using DelightGenie.Exporting.Excel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DelightGenie.Tests.Tests
{
    [TestClass]
    public class ExportingTests
    {
        [TestMethod]
        public void ExportPdf()
        {
            ExportToExcel.Instance.GenerateReport(DalUnitOfWork.Instance.CommitsRepository.GetLastCommit().Id);
        }
    }
}
