﻿using DelightGenie.DAL;
using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Models.DbModels.ProductModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DelightGenie.Tests.Tests
{
    [TestClass]
    public class UserTests
    {
        [TestMethod]
        public void PopulateGroups()
        {
            DalUnitOfWork.Instance.GroupsRepository.Insert("group1", 50);
            DalUnitOfWork.Instance.GroupsRepository.Insert("group2", 50);
            DalUnitOfWork.Instance.GroupsRepository.Insert("group3", 50);
        }

        [TestMethod]
        public void PopulateUsers()
        {
            var group = DalUnitOfWork.Instance.GroupsRepository.GetGroup("group1");

            DalUnitOfWork.Instance.UserAccountsRepository.Insert(new Employee
            {
                UserName = "user1",
                FirstName = "fn1",
                LastName = "ln1",
                Email = "email1",
                Group = group,
                WelcomeMessage = "ceva",
                Points = group.Points
            });

            DalUnitOfWork.Instance.UserAccountsRepository.Insert(new Employee
            {
                UserName = "user2",
                FirstName = "fn2",
                LastName = "ln2",
                Email = "email2",
                Group = group,
                WelcomeMessage = "ceva",
                Points = group.Points
            });

            DalUnitOfWork.Instance.UserAccountsRepository.Insert(new Employee
            {
                UserName = "user3",
                FirstName = "fn3",
                LastName = "ln3",
                Email = "email3",
                Group = group,
                WelcomeMessage = "ceva",
                Points = group.Points
            });
        }

        [TestMethod]
        public void PopulateCategories()
        {
            DalUnitOfWork.Instance.CategoriesRepository.Insert("category1");
            DalUnitOfWork.Instance.CategoriesRepository.Insert("category2");
            DalUnitOfWork.Instance.CategoriesRepository.Insert("category3");
            DalUnitOfWork.Instance.CategoriesRepository.Insert("category4");
        }

        [TestMethod]
        public void PopulateProducts()
        {
            var categ = DalUnitOfWork.Instance.CategoriesRepository.GetCategory("category1");

            DalUnitOfWork.Instance.ProductsRepository.Insert(new Product
            {
                Category = categ,
                ProductName = "prod1",
                ProductDescription = "desc1",
                ProductPrice = 120
            });

            DalUnitOfWork.Instance.ProductsRepository.Insert(new Product
            {
                Category = categ,
                ProductName = "prod2",
                ProductDescription = "desc2",
                ProductPrice = 120
            });

            DalUnitOfWork.Instance.ProductsRepository.Insert(new Product
            {
                Category = categ,
                ProductName = "prod3",
                ProductDescription = "desc3",
                ProductPrice = 120
            });
        }

        [TestMethod]
        public void PopulateTransactions()
        {
            var employee1 = DalUnitOfWork.Instance.UserAccountsRepository.GetEmployee("user1");
            var employee2 = DalUnitOfWork.Instance.UserAccountsRepository.GetEmployee("user2");
            var employee3 = DalUnitOfWork.Instance.UserAccountsRepository.GetEmployee("user3");
            var product1 = DalUnitOfWork.Instance.ProductsRepository.GetProduct("prod1");
            var product2 = DalUnitOfWork.Instance.ProductsRepository.GetProduct("prod2");
            var product3 = DalUnitOfWork.Instance.ProductsRepository.GetProduct("prod3");

            DalUnitOfWork.Instance.TransactionsRepository.Insert(35, product1, employee1);
            DalUnitOfWork.Instance.TransactionsRepository.Insert(35, product1, employee2);
            DalUnitOfWork.Instance.TransactionsRepository.Insert(180, product1, employee3);
            DalUnitOfWork.Instance.TransactionsRepository.Insert(10, product2, employee2);
            DalUnitOfWork.Instance.TransactionsRepository.Insert(10, product3, employee3);
            DalUnitOfWork.Instance.TransactionsRepository.Insert(25, product2, employee2);

            DalUnitOfWork.Instance.TransactionsRepository.Insert(35, product1, employee1);
            DalUnitOfWork.Instance.TransactionsRepository.Insert(35, product1, employee2);
            DalUnitOfWork.Instance.TransactionsRepository.Insert(180, product1, employee3);
            DalUnitOfWork.Instance.TransactionsRepository.Insert(10, product2, employee3);
            DalUnitOfWork.Instance.TransactionsRepository.Insert(10, product3, employee3);
            DalUnitOfWork.Instance.TransactionsRepository.Insert(25, product2, employee2);

            DalUnitOfWork.Instance.TransactionsRepository.Insert(35, product1, employee1);
            DalUnitOfWork.Instance.TransactionsRepository.Insert(35, product1, employee2);
            DalUnitOfWork.Instance.TransactionsRepository.Insert(180, product1, employee3);
            DalUnitOfWork.Instance.TransactionsRepository.Insert(10, product2, employee1);
            DalUnitOfWork.Instance.TransactionsRepository.Insert(10, product3, employee3);
            DalUnitOfWork.Instance.TransactionsRepository.Insert(25, product2, employee3);
        }

        [TestMethod]
        public void PopulateCommits()
        {
            DalUnitOfWork.Instance.CommitsRepository.Insert(3000);
            DalUnitOfWork.Instance.CommitsRepository.Insert(4000);
            DalUnitOfWork.Instance.CommitsRepository.Insert(5000);
            DalUnitOfWork.Instance.CommitsRepository.Insert(6000);
            DalUnitOfWork.Instance.CommitsRepository.Insert(2500);
        }

        [TestMethod]
        public void PopulatePurchases()
        {
            var commits = DalUnitOfWork.Instance.CommitsRepository.GetAll();
            var product1 = DalUnitOfWork.Instance.ProductsRepository.GetProduct("prod1");
            var product2 = DalUnitOfWork.Instance.ProductsRepository.GetProduct("prod2");
            var product3 = DalUnitOfWork.Instance.ProductsRepository.GetProduct("prod3");

            DalUnitOfWork.Instance.PurchasesRepository.Insert(5, 3, 35, commits[0], product1);
            DalUnitOfWork.Instance.PurchasesRepository.Insert(5, 2, 35, commits[0], product3);
            DalUnitOfWork.Instance.PurchasesRepository.Insert(5, 4, 65, commits[0], product3);
            DalUnitOfWork.Instance.PurchasesRepository.Insert(5, 3, 35, commits[0], product2);
            DalUnitOfWork.Instance.PurchasesRepository.Insert(5, 2, 15, commits[0], product2);
            DalUnitOfWork.Instance.PurchasesRepository.Insert(5, 4, 36, commits[0], product3);

            DalUnitOfWork.Instance.PurchasesRepository.Insert(5, 3, 35, commits[1], product1);
            DalUnitOfWork.Instance.PurchasesRepository.Insert(5, 2, 39, commits[1], product1);
            DalUnitOfWork.Instance.PurchasesRepository.Insert(5, 4, 65, commits[1], product3);
            DalUnitOfWork.Instance.PurchasesRepository.Insert(5, 3, 67, commits[1], product2);
            DalUnitOfWork.Instance.PurchasesRepository.Insert(5, 2, 35, commits[1], product2);
            DalUnitOfWork.Instance.PurchasesRepository.Insert(5, 4, 40, commits[1], product1);

            DalUnitOfWork.Instance.PurchasesRepository.Insert(10, 3, 35, commits[2], product1);
            DalUnitOfWork.Instance.PurchasesRepository.Insert(10, 2, 15, commits[2], product1);
            DalUnitOfWork.Instance.PurchasesRepository.Insert(10, 4, 5, commits[2], product1);
            DalUnitOfWork.Instance.PurchasesRepository.Insert(10, 3, 35, commits[2], product2);
            DalUnitOfWork.Instance.PurchasesRepository.Insert(10, 2, 3, commits[2], product2);
            DalUnitOfWork.Instance.PurchasesRepository.Insert(10, 4, 99, commits[2], product3);

            DalUnitOfWork.Instance.PurchasesRepository.Insert(10, 3, 67, commits[3], product2);
            DalUnitOfWork.Instance.PurchasesRepository.Insert(10, 2, 35, commits[3], product2);
            DalUnitOfWork.Instance.PurchasesRepository.Insert(10, 4, 40, commits[3], product1);
            DalUnitOfWork.Instance.PurchasesRepository.Insert(10, 3, 35, commits[3], product1);
            DalUnitOfWork.Instance.PurchasesRepository.Insert(10, 2, 15, commits[3], product1);

        }
    }
}
