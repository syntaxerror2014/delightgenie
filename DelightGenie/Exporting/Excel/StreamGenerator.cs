﻿using System.IO;
using OfficeOpenXml;

namespace DelightGenie.Exporting.Excel
{
    public abstract class StreamGenerator
    {
        internal static Stream GetExcelFileStream(ExcelPackage file)
        {
            try
            {
                var stream = new MemoryStream();
                file.SaveAs(stream);
                stream.Position = 0;

                return stream;
            }

            catch
            {
                return null;
            }
        }
    }
}