﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web;
using DelightGenie.DAL;
using DelightGenie.Models.DbModels.HistoryModels;
using OfficeOpenXml;

namespace DelightGenie.Exporting.Excel
{
    /// <summary>
    ///     This class is a singleton
    /// </summary>
    public class ExportToExcel : StreamGenerator
    {
        private const string SheetName = "Report";
        private const string CompanyName = "UnifiedPost";

        private const string CellCompanyName = "B1";
        private const string CellDate = "B3";
        private const string CellBudget = "B4";
        private const string CellTotal = "C6";
        private static ExportToExcel _instance;

        private static string _templateFile;

        private ExportToExcel()
        {
            _templateFile = Path.Combine(HttpContext.Current.Server.MapPath("~/Exporting/Templates/"),
                "UnifiedPost Purchases report.xlsx");
        }

        public static ExportToExcel Instance
        {
            get { return _instance ?? (_instance = new ExportToExcel()); }
        }

        public Stream GenerateReport(Guid commitId)
        {
            try
            {
                Commit baseCommit = DalUnitOfWork.Instance.CommitsRepository.Get(commitId);
                var commit = new Commit
                {
                    Purchases =
                        new Collection<Purchase>(
                            baseCommit.Purchases.Where(p => p.NumberOfItems != 0).ToArray()),
                    Budget = baseCommit.Budget,
                    CommitDate = baseCommit.CommitDate
                };

                ExcelPackage file = GenerateReportFile();

                if (file == null) return null;

                SetReportDetails(file.Workbook.Worksheets[SheetName], commit);
                PopulateReportContent(file.Workbook.Worksheets[SheetName], commit);

                return GetExcelFileStream(file);
            }

            catch
            {
                return null;
            }
        }

        private static ExcelPackage GenerateReportFile()
        {
            try
            {
                byte[] fileData = File.ReadAllBytes(_templateFile);

                var fileStream = new MemoryStream(fileData);

                return new ExcelPackage(fileStream);
            }

            catch (Exception)
            {
                return null;
            }
        }

        private static void SetReportDetails(ExcelWorksheet editor, Commit commit)
        {
            editor.Cells[CellCompanyName].Value = CompanyName;
            editor.Cells[CellDate].Value = commit.CommitDate.Value.ToShortDateString();
            editor.Cells[CellBudget].Value = commit.Budget;
        }

        private static void PopulateReportContent(ExcelWorksheet editor, Commit commit)
        {
            const int startIndex = 9;

            var total = (float) 0;

            if (commit.Purchases == null || commit.Purchases.Count == 0) return;

            for (int i = 0; i < commit.Purchases.Count; i++)
            {
                editor.Cells[startIndex + i, 1].Value = i + 1;
                editor.Cells[startIndex + i, 2].Value = commit.Purchases[i].Product.ProductName;
                editor.Cells[startIndex + i, 3].Value = commit.Purchases[i].ItemPrice;
                editor.Cells[startIndex + i, 4].Value = commit.Purchases[i].NumberOfItems;
                editor.Cells[startIndex + i, 5].Value = commit.Purchases[i].ItemPrice*commit.Purchases[i].NumberOfItems;

                total += commit.Purchases[i].ItemPrice*commit.Purchases[i].NumberOfItems;
            }

            editor.Cells[CellTotal].Value = total;
        }
    }
}