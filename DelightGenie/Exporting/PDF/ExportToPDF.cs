﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using DelightGenie.DAL;
using DelightGenie.Models.DbModels.HistoryModels;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace DelightGenie.Exporting.PDF
{
    public class ExportToPdf
    {
        public static byte[] GenerateReport(Guid commitId)
        {
            try
            {
                Commit baseCommit = DalUnitOfWork.Instance.CommitsRepository.Get(commitId);
                var commit = new Commit
                {
                    Purchases =
                        new Collection<Purchase>(
                            baseCommit.Purchases.Where(p => p.NumberOfItems != 0).ToArray()),
                    Budget = baseCommit.Budget,
                    CommitDate = baseCommit.CommitDate
                };

                using (var mstream = new MemoryStream())
                {
                    var doc = new Document();
                    PdfWriter pdfWriter = PdfWriter.GetInstance(doc, mstream);

                    doc.Open();

                    // Setting the content of the pdf
                    SetDocumentDetails(doc, commit);
                    PopulateDocumentWithData(doc, commit);

                    // Close the document
                    doc.Close();
                    pdfWriter.Flush();

                    // Return the content of the stream
                    return mstream.GetBuffer();
                }
            }

            catch
            {
                return null;
            }
        }

        private static void SetDocumentDetails(Document doc, Commit commit)
        {
            doc.Add(new Paragraph("Company: UnifiedPost\n\n"));
            // ReSharper disable once PossibleInvalidOperationException
            doc.Add(new Paragraph("Commit date: " + commit.CommitDate.Value.Date.ToShortDateString()));
            doc.Add(new Paragraph("Commit budget: " + commit.Budget));
        }

        private static void PopulateDocumentWithData(Document doc, Commit commit)
        {
            var total = (float)0;

            var table = new PdfPTable(5);
            table = CreateHeaderCells(table);

            for (int i = 0; i < commit.Purchases.Count; i++)
            {
                table.AddCell(new PdfPCell
                {
                    Phrase = new Phrase((i + 1).ToString()),
                    HorizontalAlignment = 1,
                    Colspan = 1
                });
                table.AddCell(new PdfPCell
                {
                    Phrase = new Phrase(commit.Purchases[i].Product.ProductName),
                    HorizontalAlignment = 1,
                    Colspan = 1
                });
                table.AddCell(new PdfPCell
                {
                    Phrase = new Phrase(commit.Purchases[i].ItemPrice.ToString()),
                    HorizontalAlignment = 1,
                    Colspan = 1
                });
                table.AddCell(new PdfPCell
                {
                    Phrase = new Phrase(commit.Purchases[i].NumberOfItems.ToString()),
                    HorizontalAlignment = 1,
                    Colspan = 1
                });
                table.AddCell(new PdfPCell
                {
                    Phrase = new Phrase((commit.Purchases[i].NumberOfItems * commit.Purchases[i].ItemPrice).ToString()),
                    HorizontalAlignment = 1,
                    Colspan = 1
                });

                total += commit.Purchases[i].NumberOfItems * commit.Purchases[i].ItemPrice;
            }

            doc.Add(new Paragraph("\nTOTAL: " + total + "\n\n"));
            doc.Add(table);
        }

        private static PdfPTable CreateHeaderCells(PdfPTable table)
        {
            var nrCell = new PdfPCell { HorizontalAlignment = 1, Colspan = 1, Phrase = new Phrase("Nr.") };
            var productCell = new PdfPCell { HorizontalAlignment = 1, Colspan = 1, Phrase = new Phrase("Product Name") };
            var priceCell = new PdfPCell { HorizontalAlignment = 1, Colspan = 1, Phrase = new Phrase("Item Price") };
            var nrOfItemsCell = new PdfPCell { HorizontalAlignment = 1, Colspan = 1, Phrase = new Phrase("Nr. of Items") };
            var totalCell = new PdfPCell { HorizontalAlignment = 1, Colspan = 1, Phrase = new Phrase("Total Price") };

            table.AddCell(nrCell);
            table.AddCell(productCell);
            table.AddCell(priceCell);
            table.AddCell(nrOfItemsCell);
            table.AddCell(totalCell);

            return table;
        }
    }
}