﻿using System.Globalization;
using System.Security.Cryptography;
using System.Text;

namespace DelightGenie.Special
{
    public class TokenGenerator
    {
        public static string EncryptMD5(string key, string type)
        {
            MD5 cypher = MD5.Create();

            // Calculate MD5 hash from input
            byte[] inputBytes = Encoding.ASCII.GetBytes(key);
            byte[] hash = cypher.ComputeHash(inputBytes);

            // Convert byte array to HEX string
            var sb = new StringBuilder();

            foreach (byte part in hash)
            {
                sb.Append(part.ToString(type));
            }

            return sb.ToString();
        }

        public static string EncryptMD5(string key)
        {
            return EncryptMD5(key, "X2");
        }

        public static string ExtractSpecialHashKey(object obj)
        {
            return obj == null ? null : obj.GetHashCode().ToString(CultureInfo.InvariantCulture);
        }
    }
}