﻿using System;
using System.Linq;
using System.Web.Mvc;
using DelightGenie.DAL;
using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Models.DbModels.NotificationModels;
using DelightGenie.Models.DbModels.ProductModels;
using DelightGenie.Models.ViewModels.AdminVM;
using DelightGenie.Special;

namespace DelightGenie.Controllers
{
    public class ProductsController : Controller
    {
        //
        // GET: /Products/
        public ActionResult Index()
        {
            if (MySession.Current.UserDetails == null ||
                (MySession.Current.UserDetails = new AccountController().GetUserDetails()) == null)
                return PartialView();

            var model = new ProductsVM
            {
                Categories = DalUnitOfWork.Instance.CategoriesRepository.GetAll()
            };

            return PartialView(model);
        }

        public ActionResult NewProduct(string id)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();
            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return Content("<h1>Access denided!</h1>");

            ProductCreateNotification productDetail = null;
            if (id != null)
                productDetail = DalUnitOfWork.Instance.NotificationsRepository.GetProductCreateNotification(new Guid(id));

            try
            {
                var model = new ProductVM();
                if (productDetail != null)
                {
                    model.Name = productDetail.ProductName;
                    model.NotificationId = productDetail.Id.ToString();
                }
                model.Categories = DalUnitOfWork.Instance.CategoriesRepository.GetAll();

                return PartialView(model);
            }
            catch (Exception)
            {
                return Content("<h1>Bad things happened!</h1>");
            }
        }

        public string EditProduct(ProductVM model)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();
            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return "F";

            var productDetails = DalUnitOfWork.Instance.ProductsRepository.Get(new Guid(model.Id));

            if (model.Name == null || model.Name.Length < 2)
                return "N";

            if (model.Price <= 0) return "P";

            productDetails.ProductName = model.Name;
            productDetails.ProductDescription = model.Description;
            productDetails.ProductPrice = model.Price;
            productDetails.Category = DalUnitOfWork.Instance.CategoriesRepository.Get(new Guid(model.Category));

            return DalUnitOfWork.Instance.ProductsRepository.Update(productDetails) != null ? "K" : "F";
        }

        public string CreateProduct(ProductVM model)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return "F";

            var productDetails = new Product();

            if (model.Name == null || model.Name.Length < 2)
                return "N";

            if (model.Price <= 0) return "P";

            productDetails.ProductName = model.Name;
            productDetails.ProductDescription = model.Description;
            productDetails.ProductPrice = model.Price;
            productDetails.Category = DalUnitOfWork.Instance.CategoriesRepository.Get(new Guid(model.Category));

            if (!string.IsNullOrEmpty(model.NotificationId))
                DalUnitOfWork.Instance.NotificationsRepository.Delete(new Guid(model.NotificationId));

            return DalUnitOfWork.Instance.ProductsRepository.Insert(productDetails) != null ? "K" : "F";
        }

        public ActionResult ProductsTable(string categoryId)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return PartialView();

            try
            {
                if (categoryId == "0")
                {
                    var model = new ProductsVM
                    {
                        Products = DalUnitOfWork.Instance.ProductsRepository.GetAll()
                    };
                    return PartialView(model);
                }
                else
                {
                    var model = new ProductsVM
                    {
                        Products = DalUnitOfWork.Instance.CategoriesRepository.GetProducts(new Guid(categoryId))
                    };
                    return PartialView(model);
                }
            }
            catch (Exception)
            {
                return PartialView();
                ;
            }
        }

        public ActionResult GetProduct(string id)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return Content("<h1>Access denided!</h1>");

            try
            {
                var product = DalUnitOfWork.Instance.ProductsRepository.Get(new Guid(id));
                var model = new ProductVM
                {
                    Id = id,
                    Name = product.ProductName,
                    Description = product.ProductDescription,
                    Category = product.Category.Id.ToString(),
                    Price = product.ProductPrice,
                    Categories = DalUnitOfWork.Instance.CategoriesRepository.GetAll()
                };

                return PartialView(model);
            }
            catch (Exception)
            {
                return Content("<h1>Inexistent product!</h1>");
            }
        }

        public string DeleteProduct(string id)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return "F";

            return DalUnitOfWork.Instance.ProductsRepository.Delete(new Guid(id)) ? "K" : "F";
        }

        public ActionResult GetProducts(string categoryId)
        {
            if (MySession.Current.UserDetails == null ||
                (MySession.Current.UserDetails = new AccountController().GetUserDetails()) == null)
                return Json(null, JsonRequestBehavior.AllowGet);
            try
            {
                if (categoryId == "0")
                {
                    var products =
                    DalUnitOfWork.Instance.ProductsRepository.GetAll().Select(p => new
                    {
                        p.Id,
                        p.ProductName,
                        p.ProductDescription,
                        p.ProductPrice,
                        p.Category.CategoryName
                    });
                    return Json(products, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var products =
                    DalUnitOfWork.Instance.CategoriesRepository.GetProducts(new Guid(categoryId)).Select(p => new
                    {
                        p.Id,
                        p.ProductName,
                        p.ProductDescription,
                        p.ProductPrice,
                        p.Category.CategoryName
                    });
                    return Json(products, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public string RequestProduct(string productName)
        {
            if (MySession.Current.UserDetails == null ||
                (MySession.Current.UserDetails = new AccountController().GetUserDetails()) == null)
                return "F";

            return DalUnitOfWork.Instance.NotificationsRepository.InsertProductCreateNotification(productName, "",
                MySession.Current.UserDetails as Employee) != null
                ? "K"
                : "F";
        }

        [HttpPost]
        public int Vote(string productId, int points)
        {
            if (MySession.Current.UserDetails == null ||
                (MySession.Current.UserDetails = new AccountController().GetUserDetails()) == null)
                return -1;

            if (points < 1) return -2;

            var product = DalUnitOfWork.Instance.ProductsRepository.Get(new Guid(productId));
            var employee = DalUnitOfWork.Instance.UserAccountsRepository.SubstractPointsForUser(
                MySession.Current.UserDetails.Id, points);
            var transaction = DalUnitOfWork.Instance.TransactionsRepository.Insert(points, product,
                MySession.Current.UserDetails as Employee);
            var user = MySession.Current.UserDetails = new AccountController().GetUserDetails();

            return (product != null && employee != null && transaction != null && user != null)
                ? (user as Employee).Points
                : -2;
        }
    }
}