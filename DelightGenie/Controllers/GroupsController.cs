﻿using System;
using System.Web.Mvc;
using DelightGenie.DAL;
using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Models.ViewModels.AdminVM;
using DelightGenie.Special;

namespace DelightGenie.Controllers
{
    public class GroupsController : Controller
    {
        //
        // GET: /Groups/
        public ActionResult Index()
        {
            if (MySession.Current.UserDetails == null ||
                (MySession.Current.UserDetails = new AccountController().GetUserDetails()) == null)
                return PartialView();

            var model = new GroupsVM
            {
                Groups = DalUnitOfWork.Instance.GroupsRepository.GetAll()
            };

            return PartialView(model);
        }

        public string DeleteGroup(string id)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return "F";

            var group = DalUnitOfWork.Instance.GroupsRepository.Get(new Guid(id));
            if (group.GroupName == Group.DefaultGroupName)
                return "F";

            return DalUnitOfWork.Instance.GroupsRepository.Delete(group) ? "K" : "F";
        }

        [HttpPost]
        public string InsertGroup(string groupName, int points)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return "F";

            if (DalUnitOfWork.Instance.GroupsRepository.VerifyExists(groupName))
                return "D";

            return DalUnitOfWork.Instance.GroupsRepository.Insert(groupName, points) != null ? "K" : "F";
        }

        [HttpPost]
        public string EditGroup(GroupVM model)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();
            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return "F";

            var group = DalUnitOfWork.Instance.GroupsRepository.Get(new Guid(model.Id));

            if (model.Name == null || model.Name.Length < 3)
                return "N";

            int pcts;

            if (model.Points == null || int.TryParse(model.Points, out pcts) == false || pcts < 0)
                return "P";

            if (group.GroupName != model.Name && DalUnitOfWork.Instance.GroupsRepository.VerifyExists(model.Name))
                return "D";

            group.Points = pcts;
            if (group.GroupName != Group.DefaultGroupName)
                group.GroupName = model.Name;

            return DalUnitOfWork.Instance.GroupsRepository.Update(group) != null ? "K" : "F";
        }

        public ActionResult GetGroup(string id)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();
            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return Content("<h1>Access denided!</h1>");

            var group = DalUnitOfWork.Instance.GroupsRepository.Get(new Guid(id));
            var model = new GroupVM
            {
                Name = group.GroupName,
                Id = group.Id.ToString(),
                Points = group.Points.ToString()
            };

            return PartialView(model);
        }
    }
}