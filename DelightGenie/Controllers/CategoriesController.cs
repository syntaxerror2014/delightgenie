﻿using System;
using System.Linq;
using System.Web.Mvc;
using DelightGenie.DAL;
using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Models.ViewModels.AdminVM;
using DelightGenie.Special;

namespace DelightGenie.Controllers
{
    public class CategoriesController : Controller
    {
        //
        // GET: /Categories/
        public ActionResult Index()
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return PartialView();

            var model = new CategoriesVM
            {
                Categories = DalUnitOfWork.Instance.CategoriesRepository.GetAll()
            };

            return PartialView(model);
        }

        public ActionResult Categories()
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return PartialView();

            var model = new CategoriesVM
            {
                Categories = DalUnitOfWork.Instance.CategoriesRepository.GetAll()
            };

            return PartialView(model);
        }

        public string DeleteCategory(string id)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return "F";

            return DalUnitOfWork.Instance.CategoriesRepository.Delete(new Guid(id)) ? "K" : "F";
        }

        [HttpPost]
        public string InsertCategory(string categoryName)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return "F";

            return DalUnitOfWork.Instance.CategoriesRepository.Insert(categoryName) != null ? "K" : "F";
        }

        [HttpPost]
        public string EditCategory(CategoryVM model)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return "F";

            var category = DalUnitOfWork.Instance.CategoriesRepository.Get(new Guid(model.Id));
            category.CategoryName = model.Name;

            return DalUnitOfWork.Instance.CategoriesRepository.Update(category) != null ? "K" : "F";
        }

        public ActionResult GetCategory(string id)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();
            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return Content("<h1>Access denided!</h1>");

            var category = DalUnitOfWork.Instance.CategoriesRepository.Get(new Guid(id));
            var model = new CategoryVM
            {
                Id = category.Id.ToString(),
                Name = category.CategoryName
            };

            return PartialView(model);
        }

        public ActionResult GetCategories()
        {
            if (MySession.Current.UserDetails == null ||
                (MySession.Current.UserDetails = new AccountController().GetUserDetails()) == null)
                return Json(null, JsonRequestBehavior.AllowGet);

            try
            {
                var categories = DalUnitOfWork.Instance.CategoriesRepository.GetAll().Select(p => new
                {
                    p.Id,
                    p.CategoryName
                });

                return Json(categories, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
    }
}