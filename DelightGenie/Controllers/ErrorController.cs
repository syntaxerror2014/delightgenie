﻿using System.Web.Mvc;

namespace DelightGenie.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error404/
        public ActionResult Error404()
        {
            return View();
        }
    }
}