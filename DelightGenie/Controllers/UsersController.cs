﻿using System;
using System.Web.Mvc;
using DelightGenie.DAL;
using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Models.DbModels.NotificationModels;
using DelightGenie.Models.ViewModels.AdminVM;
using DelightGenie.Special;

namespace DelightGenie.Controllers
{
    public class UsersController : Controller
    {
        //
        // GET: /Users/
        public ActionResult Index()
        {
            if (MySession.Current.UserDetails == null ||
                (MySession.Current.UserDetails = new AccountController().GetUserDetails()) == null)
                return PartialView();

            var model = new GroupsVM
            {
                Groups = DalUnitOfWork.Instance.GroupsRepository.GetAll()
            };

            return PartialView(model);
        }

        public ActionResult UsersTable(string groupId)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return PartialView();

            try
            {
                if (groupId == "0")
                {
                    var model = new UsersVM
                    {
                        Employees = DalUnitOfWork.Instance.UserAccountsRepository.GetAllEmployees()
                    };
                    return PartialView(model);
                }
                else
                {
                    var model = new UsersVM
                    {
                        Employees = DalUnitOfWork.Instance.GroupsRepository.GetUsersForGroup(new Guid(groupId))
                    };
                    return PartialView(model);
                }
            }
            catch (Exception)
            {
                return PartialView();
            }
        }

        public ActionResult GetUser(string id)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return Content("<h1>Access denided!</h1>");

            try
            {
                var employee = DalUnitOfWork.Instance.UserAccountsRepository.GetEmployee(id);
                var model = new UserVM
                {
                    UserName = id,
                    OldUserName = id,
                    FirstName = employee.FirstName,
                    LastName = employee.LastName,
                    EmailAddress = employee.Email,
                    Group = employee.Group.Id.ToString(),
                    Groups = DalUnitOfWork.Instance.GroupsRepository.GetAll(),
                    Points = employee.Points
                };

                return PartialView(model);
            }
            catch (Exception)
            {
                return Content("<h1>Inexistent user!</h1>");
            }
        }

        public string EditUser(UserVM model)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return "F";

            Employee userProfile =
                DalUnitOfWork.Instance.UserAccountsRepository.GetEmployee(model.OldUserName);

            if ((model.UserName == null || model.UserName.Length < 4 || model.UserName.Length > 28 ||
                 !Char.IsLetter(model.UserName[0]) ||
                 DalUnitOfWork.Instance.UserAccountsRepository.VerifyExists(model.UserName)) &&
                userProfile.UserName != model.UserName)
                return "U";

            userProfile.UserName = model.UserName;
            userProfile.FirstName = model.FirstName;
            userProfile.LastName = model.LastName;
            userProfile.Email = model.EmailAddress;
            userProfile.Group = DalUnitOfWork.Instance.GroupsRepository.Get(new Guid(model.Group));
            userProfile.UserName = model.UserName;
            userProfile.Points = model.Points;

            return DalUnitOfWork.Instance.UserAccountsRepository.Update(userProfile) != null ? "K" : "F";
        }

        [HttpPost]
        public string ChangeAdminPass(SettingsVM model)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return "F";

            if (!DalUnitOfWork.Instance.UserAccountsRepository.VerifyPassword("admin", model.Password))
                return "P";

            if (model.NewPassword == null || model.NewPassword.Length < 4 || model.NewPassword.Length > 28 ||
                model.NewPassword != model.NewPasswordAgain)
                return "N";

            Admin admin = DalUnitOfWork.Instance.UserAccountsRepository.GetAdmin();
            admin.Password = TokenGenerator.EncryptMD5(model.NewPassword);

            return DalUnitOfWork.Instance.UserAccountsRepository.Update(admin) != null ? "K" : "F";
        }

        public string DeleteUser(string id)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return "F";

            return DalUnitOfWork.Instance.UserAccountsRepository.Delete(id) ? "K" : "F";
        }

        public ActionResult NewUser(string id)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return Content("<h1>Access denided!</h1>");

            AccountCreateNotification userDetail = null;
            if (id != null)
                userDetail = DalUnitOfWork.Instance.NotificationsRepository.GetAccountCreateNotification(new Guid(id));

            try
            {
                var model = new UserVM();
                if (userDetail != null)
                {
                    model.EmailAddress = userDetail.Email;
                    model.FirstName = userDetail.FirstName;
                    model.LastName = userDetail.LastName;
                }
                model.Groups = DalUnitOfWork.Instance.GroupsRepository.GetAll();

                return PartialView(model);
            }
            catch (Exception)
            {
                return Content("<h1>Bad things happened!</h1>");
            }
        }
    }
}