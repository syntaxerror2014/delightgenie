﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using DelightGenie.DAL;
using DelightGenie.Exporting.Excel;
using DelightGenie.Exporting.PDF;
using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Models.DbModels.HistoryModels;
using DelightGenie.Models.ViewModels.AdminVM;
using DelightGenie.Special;
using WebGrease.Css.Extensions;

namespace DelightGenie.Controllers
{
    public class ReportsController : Controller
    {
        //
        // GET: /Reports/
        public ActionResult CreateTransactionCharts(string id)
        {
            Commit commit = !string.IsNullOrEmpty(id)
                ? DalUnitOfWork.Instance.CommitsRepository.Get(new Guid(id))
                : null;

            if (commit != null)
            {
                Collection<Purchase> transactions = commit.Purchases;

                if (transactions == null)
                    return Json(null, JsonRequestBehavior.AllowGet);

                var data = transactions
                    .Select(t => new
                    {
                        t.Product.ProductName,
                        t.Points,
                    });

                return Json(data, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        public ActionResult CreatePurchasesTimeline()
        {
            Commit[] commits = DalUnitOfWork.Instance.CommitsRepository.GetAll();

            var data = new PurchaseChartObject[commits.Length];

            for (int i = 0; i < commits.Length; i++)
            {
                Collection<Purchase> purchases = commits[i].Purchases;

                data[i] = new PurchaseChartObject
                {
                    Date = commits[i].CommitDate,
                    Budget = commits[i].Budget,
                    Paid = purchases != null
                        ? purchases.Sum(p => p.ItemPrice*p.NumberOfItems)
                        : 0
                };
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateTransactionsTimeline()
        {
            var commits = DalUnitOfWork.Instance.CommitsRepository.GetAll();

            var products = DalUnitOfWork.Instance.ProductsRepository.GetAll();

            if (commits.Length == 0)
                return null;

            var data = new TimelineObject[commits.Length];

            for (int i = 0; i < commits.Length; i++)
            {
                IEnumerable<Transaction> transactions =
                    CompressDuplicates(DalUnitOfWork.Instance.TransactionsRepository.GetTransactionsForCommit(commits[i]));

                var points = new int[products.Length];

                points.Initialize();

                if (transactions != null)
                {
                    transactions.ForEach(t =>
                    {
                        for (int j = 0; j < products.Length; j++)
                        {
                            if (t.Product.ProductName != products[j].ProductName) continue;

                            points[j] = t.Points;

                            break;
                        }
                    });
                }

                data[i] = new TimelineObject
                {
                    Date = commits[i].CommitDate,
                    Products = products.Select(p => p.ProductName).ToArray(),
                    Points = points
                };
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetReportXls(string id)
        {
            if (MySession.Current.UserDetails == null ||
                (MySession.Current.UserDetails = new AccountController().GetUserDetails()) == null)
                return Redirect("/Home/Index");
            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return Redirect("/Home/Index/#notadmin");

            Stream stream = ExportToExcel.Instance.GenerateReport(new Guid(id));
            return File(stream, "application/xlsx", "Report.xlsx");
        }

        public ActionResult GetReportPdf(string id)
        {
            if (MySession.Current.UserDetails == null ||
                (MySession.Current.UserDetails = new AccountController().GetUserDetails()) == null)
                return Redirect("/Home/Index");
            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return Redirect("/Home/Index/#notadmin");

            byte[] stream = ExportToPdf.GenerateReport(new Guid(id));
            return File(stream, "application/pdf", "Report.pdf");
        }

        public ActionResult GraphsForCommit()
        {
            if (MySession.Current.UserDetails == null ||
                (MySession.Current.UserDetails = new AccountController().GetUserDetails()) == null)
                return Redirect("/Home/Index");
            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return Redirect("/Home/Index/#notadmin");

            var model = new CommitsVM
            {
                Commits =
                    DalUnitOfWork.Instance.CommitsRepository.GetAll().OrderByDescending(p => p.CommitDate).ToArray()
            };

            return PartialView(model);
        }

        public ActionResult TransactionsTimeline()
        {
            return View();
        }

        public ActionResult PurchasesTimeline()
        {
            return View();
        }

        #region private methods

        private IEnumerable<Transaction> CompressDuplicates(
            ICollection<Transaction> transactions)
        {
            IList<Transaction> result = new List<Transaction>();

            if (transactions.Count == 0)
                return null;

            var aux = transactions.ElementAt(0);
            result.Add(aux);

            for (var i = 1; i < transactions.Count; i++)
            {
                var t = transactions.ElementAt(i);

                if (t.Product.ProductName == aux.Product.ProductName)
                    aux.Points += t.Points;

                else
                {
                    result.Add(t);
                    aux = t;
                }
            }

            return result;
        }

        #endregion

        #region auxiliary classes

        private class PurchaseChartObject
        {
            public DateTime? Date { get; set; }

            public float Budget { get; set; }

            public float Paid { get; set; }
        }

        private class TimelineObject
        {
            public DateTime? Date { get; set; }

            public string[] Products { get; set; }

            public int[] Points { get; set; }
        }

        #endregion
    }
}