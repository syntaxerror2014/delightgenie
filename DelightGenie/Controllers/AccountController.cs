﻿using System;
using System.DirectoryServices;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Web.Mvc;
using DelightGenie.DAL;
using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Models.ViewModels;
using DelightGenie.Special;

namespace DelightGenie.Controllers
{
    public class AccountController : Controller
    {
        private readonly CookieHelper _myCookie;

        public AccountController()
        {
            _myCookie = new CookieHelper();
        }

        /// <summary>
        ///     Test if the specified email is valid
        /// </summary>
        /// <param name="email">The email string to be tested</param>
        /// <returns>Whether the email is valid or not</returns>
        public bool IsValidEmail(string email)
        {
            try
            {
                // ReSharper disable once ObjectCreationAsStatement
                new MailAddress(email);

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        ///     Test if the user exists on the LDAP server
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool LDAPAutentification(string userName, string password)
        {
            //string domain = "ldap.testathon.net:389/ou=Users,dc=testathon,dc=net";
            //string pstrUser = "CN="+userName+",OU=Users,DC=testathon,DC=net";
            const string domain = "ldap01.csn.dc01.uni:389/ou=People,dc=um,dc=unifiedpost,dc=com";
            string pstrUser = "uid=" + userName + ",ou=People,dc=um,dc=unifiedpost,dc=com";
            try
            {
                using (
                    var objADEntry = new DirectoryEntry("LDAP://" + domain, pstrUser, password, AuthenticationTypes.None)
                    )
                {
                    return !objADEntry.NativeObject.Equals(null);
                }
            }
            catch (COMException)
            {
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        ///     Log the user in
        /// </summary>
        /// <param name="model"></param>
        /// <returns>A string value used in javascript checking</returns>
        [HttpPost]
        public string Login(LoginModel model)
        {
            if (model.UserName.ToLower() == "admin") //Check whether the admin will log in
            {
                if (!DalUnitOfWork.Instance.UserAccountsRepository.VerifyPassword(model.UserName, model.Password))
                    return "F";
            }
            else
            {
                if (!LDAPAutentification(model.UserName, model.Password))
                    return "F";

                if (!DalUnitOfWork.Instance.UserAccountsRepository.VerifyExists(model.UserName))
                {
                    Group group = DalUnitOfWork.Instance.GroupsRepository.GetGroup("Default");

                    var newUser = new Employee
                    {
                        UserName = model.UserName,
                        Group = group,
                        Points = group.Points
                    };

                    // The password is automatically encrypted in the model
                    if (DalUnitOfWork.Instance.UserAccountsRepository.Insert(newUser) == null) return "F";
                }
            }

            //Make a new session for the user
            Session newSession = DalUnitOfWork.Instance.SessionsRepository.Login(model.UserName);

            //Create the cookies for the session
            _myCookie.SetCookie("sessionId", newSession.Id.ToString(), DateTime.Now.AddMonths(6));
            _myCookie.SetCookie("sessionKey", newSession.SessionKey, DateTime.Now.AddMonths(6));

            return "K";
        }

        /// <summary>
        ///     Log out the currently logged in user
        /// </summary>
        /// <returns></returns>
        public string LogOut()
        {
            //Delete the cookies
            _myCookie.DeleteCookie("sessionId");
            _myCookie.DeleteCookie("sessionKey");

            //Delete the session from the server
            try
            {
                //Delete the session variable but remember the userName
                string userName = MySession.Current.UserDetails.UserName;
                MySession.Current.UserDetails = null;

                return DalUnitOfWork.Instance.SessionsRepository.Logout(userName) ? "K" : "F";
            }
            catch (Exception)
            {
                return "F";
            }
        }

        /// <summary>
        ///     Get the details for the currently logged in user
        /// </summary>
        /// <returns>Returns an UserAccount model</returns>
        public UserAccount GetUserDetails()
        {
            if (_myCookie.GetCookie("sessionId") == "" || _myCookie.GetCookie("sessionKey") == "")
                return null;

            var user = DalUnitOfWork.Instance.SessionsRepository.GetUser(
                new Guid(_myCookie.GetCookie("sessionId")), _myCookie.GetCookie("sessionKey"));

            if (user == null)
            {
                _myCookie.DeleteCookie("sessionId");
                _myCookie.DeleteCookie("sessionKey");
            }

            return user;
        }


        //To be deleted
    }
}