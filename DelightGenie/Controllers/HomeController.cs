﻿using System.Web.Mvc;
using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Special;

namespace DelightGenie.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index(int asPartial = 0)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (asPartial == 1) return PartialView();
            return View();
        }

        public ActionResult Vote(int asPartial = 0)
        {
            if (MySession.Current.UserDetails == null ||
                (MySession.Current.UserDetails = new AccountController().GetUserDetails()) == null ||
                MySession.Current.UserDetails is Admin)
                return Redirect("/Home/Index/#login");

            if (asPartial == 1) return PartialView();
            return View();
        }

        public ActionResult Dashboard(int asPartial = 0)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return Redirect("/Home/Index/#notadmin");

            if (asPartial == 1) return PartialView();
            return View();
        }
    }
}