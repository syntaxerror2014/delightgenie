﻿using System;
using System.Web.Mvc;
using DelightGenie.DAL;
using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Models.DbModels.HistoryModels;
using DelightGenie.Models.ViewModels.AdminVM;
using DelightGenie.Special;

namespace DelightGenie.Controllers
{
    public class TransactionsController : Controller
    {
        public ActionResult TransactionsTable()
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (!(MySession.Current.UserDetails is Employee))
                return View();

            var model = new TransactionsVM();

            model.Transactions =
                DalUnitOfWork.Instance.TransactionsRepository.GetUncommitedTransactionsForEmployee(
                    (Employee) MySession.Current.UserDetails);

            return PartialView(model);
        }

        public string DeleteTransaction(string id)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (!(MySession.Current.UserDetails is Employee))
                return "F";

            try
            {
                Transaction transaction = DalUnitOfWork.Instance.TransactionsRepository.Get(new Guid(id));
                if (transaction.Employee != MySession.Current.UserDetails)
                    return "F";
                return DalUnitOfWork.Instance.TransactionsRepository.Delete(transaction.Id)
                    ? DalUnitOfWork.Instance.UserAccountsRepository.SubstractPointsForUser(
                        MySession.Current.UserDetails.Id, -transaction.Points).Points + ":" +
                      transaction.Id
                    : "F";
            }
            catch (Exception)
            {
                return "F";
            }
        }

        public string UpdateTransaction(string id, string points)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (!(MySession.Current.UserDetails is Employee))
                return "-1";

            Transaction transaction = DalUnitOfWork.Instance.TransactionsRepository.Get(new Guid(id));

            if (transaction.Employee != MySession.Current.UserDetails)
                return "-1";

            if (Convert.ToInt16(points) < 1) return "-3";

            Employee newEmployee =
                DalUnitOfWork.Instance.UserAccountsRepository.SubstractPointsForUser(
                    MySession.Current.UserDetails.Id, Convert.ToInt16(points) - transaction.Points);

            if (newEmployee != null)
            {
                transaction.Points = Convert.ToInt16(points);
                return DalUnitOfWork.Instance.TransactionsRepository.Update(transaction) != null
                    ? newEmployee.Points.ToString()
                    : "-4";
            }

            return "-2";
        }
    }
}