﻿using System;
using System.Web.Mvc;
using DelightGenie.DAL;
using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Models.ViewModels.AdminVM;
using DelightGenie.Special;

namespace DelightGenie.Controllers
{
    public class NotificationsController : Controller
    {
        //
        // GET: /Error404/
        public ActionResult Index()
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();

            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return View();

            var model = new NotificationsVM();

            model.notifications = DalUnitOfWork.Instance.NotificationsRepository.GetAllProductCreateNotifications();

            return View(model);
        }

        public ActionResult GetProductNotifications()
        {
            return View();
        }

        public string DeleteProductNotification(string id)
        {
            if (MySession.Current.UserDetails == null)
                MySession.Current.UserDetails = new AccountController().GetUserDetails();
            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return "F";

            return DalUnitOfWork.Instance.NotificationsRepository.Delete(new Guid(id)) ? "K" : "F";
        }
    }
}