﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using DelightGenie.DAL;
using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Models.DbModels.HistoryModels;
using DelightGenie.Models.ViewModels;
using DelightGenie.Models.ViewModels.AdminVM;
using DelightGenie.Special;

namespace DelightGenie.Controllers
{
    public class PurchasesController : Controller
    {
        //
        // GET: /Purchases/


        public ActionResult Index()
        {
            if (MySession.Current.UserDetails == null ||
                (MySession.Current.UserDetails = new AccountController().GetUserDetails()) == null)
                return Redirect("/Home/Index");
            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return Redirect("/Home/Index/#notadmin");

            var model = new CommitsVM
            {
                Commits =
                    DalUnitOfWork.Instance.CommitsRepository.GetAll().OrderByDescending(p => p.CommitDate).ToArray()
            };

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult GetCommitTable(string id)
        {
            if (MySession.Current.UserDetails == null ||
                (MySession.Current.UserDetails = new AccountController().GetUserDetails()) == null)
                return Redirect("/Home/Index");
            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return Redirect("/Home/Index/#notadmin");

            Commit commit = DalUnitOfWork.Instance.CommitsRepository.Get(new Guid(id));
            if (commit == null) return PartialView();

            var model = new PurchasesVM();
            var results = new List<ResultModel>();
            foreach (Purchase purchase in commit.Purchases)
            {
                results.Add(new ResultModel
                {
                    NrItems = purchase.NumberOfItems,
                    AllPrice = purchase.ItemPrice*purchase.NumberOfItems,
                    ProductId = purchase.Product.Id.ToString(),
                    ProductName = purchase.Product.ProductName,
                    ProductPrice = purchase.ItemPrice,
                    TotalScore = purchase.Points
                });

                model.TotalAllPrice += purchase.ItemPrice*purchase.NumberOfItems;
            }

            model.Results = results.OrderByDescending(p => p.TotalScore).ToArray();
            model.ViewExistent = true;
            model.CommitID = commit.Id.ToString();

            return PartialView("GenerateReport", model);
        }

        /// <summary>
        ///     Generate the report for the uncommited point allocations
        /// </summary>
        /// <param name="budget">The budget for this month</param>
        /// <param name="submitcommit">Whether the report should be saved as accepted 0/1</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GenerateReport(float budget, int submitcommit)
        {
            if (MySession.Current.UserDetails == null ||
                (MySession.Current.UserDetails = new AccountController().GetUserDetails()) == null)
                return Redirect("/Home/Index");
            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return Redirect("/Home/Index/#notadmin");

            try
            {
                Transaction[] transactions =
                    DalUnitOfWork.Instance.TransactionsRepository.GetTransactionsForCommit(null);
                var results = new List<ResultModel>();

                var model = new PurchasesVM { TotalAllScore = 0, TotalAllPrice = 0 };

                float fakeTotalPrice = 0;

                foreach (Transaction transaction in transactions)
                {
                    int sum = transaction.Points;
                    model.TotalAllScore += sum;
                    fakeTotalPrice += sum*transaction.Product.ProductPrice;

                    results.Add(new ResultModel
                    {
                        ProductId = transaction.Product.Id.ToString(),
                        ProductName = transaction.Product.ProductName,
                        ProductPrice = transaction.Product.ProductPrice,
                        TotalScore = sum
                    });
                }

                Commit commit = null;
                if (submitcommit == 1)
                {
                    DalUnitOfWork.Instance.CommitsRepository.Insert(budget);
                    commit = DalUnitOfWork.Instance.CommitsRepository.GetLastCommit();
                    commit.Purchases = new Collection<Purchase>();
                }

                float alpha = budget/fakeTotalPrice;
                for (int i = 0; i < results.Count; i++)
                {
                    //results[i].Percentage = (float)results[i].TotalScore / model.TotalAllScore * 100;
                    //results[i].NrItems = (int)(((results[i].Percentage / 100 * budget) / results[i].ProductPrice));

                    //Now calculate the percent from number of products

                    results[i].NrItems = (int)(alpha * results[i].TotalScore);
                    results[i].AllPrice = results[i].NrItems * results[i].ProductPrice;
                    model.TotalAllPrice += results[i].AllPrice;

                    if (commit != null && submitcommit == 1 && results[i].NrItems > 0)
                    {
                        commit.Purchases.Add(new Purchase
                        {
                            Id = Guid.NewGuid(),
                            Product = DalUnitOfWork.Instance.ProductsRepository.Get(new Guid(results[i].ProductId)),
                            ItemPrice = results[i].ProductPrice,
                            NumberOfItems = results[i].NrItems,
                            Commit = commit,
                            Points = results[i].TotalScore
                        });
                    }
                }

                if (submitcommit == 1)
                {
                    DalUnitOfWork.Instance.CommitsRepository.Update(commit);
                    foreach (var group in DalUnitOfWork.Instance.GroupsRepository.GetAll())
                    {
                        DalUnitOfWork.Instance.GroupsRepository.ResetGroupPoints(group.Id);
                    }
                }

                model.Results = results.Where(p => p.NrItems != 0).OrderByDescending(p => p.TotalScore).ToArray();

                return PartialView(model);
            }
            catch
            {
                return PartialView();
            }
        }

        public string DeleteCommit(string id)
        {
            if (MySession.Current.UserDetails == null ||
                (MySession.Current.UserDetails = new AccountController().GetUserDetails()) == null)
                return "F";
            if (MySession.Current.UserDetails == null || MySession.Current.UserDetails is Employee)
                return "F";

            if (id == null || DalUnitOfWork.Instance.CommitsRepository.Delete(new Guid(id)) == false) return "F";

            try
            {
                foreach (var employee in DalUnitOfWork.Instance.UserAccountsRepository.GetAllEmployees())
                {
                    var points = DalUnitOfWork.Instance.TransactionsRepository.GetUncommitedTransactionsForEmployee(employee).Sum(t => t.Points);
                    employee.Points = employee.Group.Points - points;
                    DalUnitOfWork.Instance.UserAccountsRepository.Update(employee);
                }
                return "K";
            }
            catch
            {
                return "F";
            }
        }
    }
}