﻿using System.Data.Entity;
using DelightGenie.Models.DbModels.HistoryModels;

namespace DelightGenie.DAL.DbContexts
{
    public class HistoryContextInitializer
    {
        public static void InitializeModels(DbModelBuilder modelBuilder)
        {
            InitializeCommitsTable(modelBuilder);
            InitializePurchasesTable(modelBuilder);
            InitializeTransactionsTable(modelBuilder);
        }

        private static void InitializeTransactionsTable(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Transaction>()
                .HasKey(k => k.Id);

            modelBuilder.Entity<Transaction>()
                .Property(p => p.TransactionDate).IsRequired();

            modelBuilder.Entity<Transaction>()
                .Property(p => p.Points).IsRequired();

            // Map foreign key to the Employees table
            modelBuilder.Entity<Transaction>()
                .HasRequired(p => p.Employee)
                .WithMany(p => p.Transactions)
                .Map(m => m.MapKey("EmployeeId"))
                .WillCascadeOnDelete(true);

            // Map foreign key to the Transactions table
            modelBuilder.Entity<Transaction>()
                .HasRequired(p => p.Product)
                .WithMany(p => p.Transactions)
                .Map(m => m.MapKey("ProductId"))
                .WillCascadeOnDelete(true);
        }

        private static void InitializeCommitsTable(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Commit>()
                .HasKey(k => k.Id);

            modelBuilder.Entity<Commit>()
                .Property(p => p.CommitDate).IsRequired();

            modelBuilder.Entity<Commit>()
                .Property(p => p.Budget).IsRequired();
        }

        private static void InitializePurchasesTable(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Purchase>()
                .HasKey(k => k.Id);

            modelBuilder.Entity<Purchase>()
                .Property(p => p.ItemPrice).IsRequired();

            modelBuilder.Entity<Purchase>()
                .Property(p => p.NumberOfItems).IsRequired();

            modelBuilder.Entity<Purchase>()
                .Property(p => p.Points).IsRequired();

            // Map foreign key to the Commits table
            modelBuilder.Entity<Purchase>()
                .HasRequired(p => p.Commit)
                .WithMany(p => p.Purchases)
                .Map(m => m.MapKey("CommitId"))
                .WillCascadeOnDelete(true);

            // Map foreign key to the Purchases table
            modelBuilder.Entity<Purchase>()
                .HasRequired(p => p.Product)
                .WithMany(p => p.Purchases)
                .Map(m => m.MapKey("ProductId"))
                .WillCascadeOnDelete(true);
        }
    }
}