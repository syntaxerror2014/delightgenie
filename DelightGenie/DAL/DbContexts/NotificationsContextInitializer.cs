﻿using System.Data.Entity;
using DelightGenie.Models.DbModels.NotificationModels;

namespace DelightGenie.DAL.DbContexts
{
    public class NotificationsContextInitializer
    {
        public static void InitializeModels(DbModelBuilder modelBuilder)
        {
            InitializeNotificationsTablePart(modelBuilder);
            InitializeCreateAccountNotificationsTablePart(modelBuilder);
            InitializeCreateProductNotificationsTablePart(modelBuilder);
            InitializePointsNotificationsTablePart(modelBuilder);

            InitializeMessagesTable(modelBuilder);
        }

        private static void InitializeNotificationsTablePart(DbModelBuilder modelBulBuilder)
        {
            modelBulBuilder.Entity<Notification>()
                .HasKey(k => k.Id);

            modelBulBuilder.Entity<Notification>()
                .Property(p => p.NotificationDate).IsRequired();

            // Map foreign key to the Employees table
            modelBulBuilder.Entity<Notification>()
                .HasOptional(p => p.Employee)
                .WithMany(p => p.Notifications)
                .Map(m => m.MapKey("EmployeeId"))
                .WillCascadeOnDelete(true);
        }

        private static void InitializeCreateAccountNotificationsTablePart(DbModelBuilder modelBulBuilder)
        {
            modelBulBuilder.Entity<AccountCreateNotification>()
                .Property(p => p.FirstName).IsRequired();

            modelBulBuilder.Entity<AccountCreateNotification>()
                .Property(p => p.LastName).IsRequired();

            modelBulBuilder.Entity<AccountCreateNotification>()
                .Property(p => p.Email).IsRequired();
        }

        private static void InitializeCreateProductNotificationsTablePart(DbModelBuilder modelBulBuilder)
        {
            modelBulBuilder.Entity<ProductCreateNotification>()
                .Property(p => p.ProductName).IsRequired();

            modelBulBuilder.Entity<ProductCreateNotification>()
                .Property(p => p.Description).IsOptional();
        }

        private static void InitializePointsNotificationsTablePart(DbModelBuilder modelBulBuilder)
        {
            // Map foreign key to the Messages table
            modelBulBuilder.Entity<PointsNotification>()
                .HasRequired(p => p.Message)
                .WithMany(p => p.PointsNotifications)
                .Map(m => m.MapKey("MessageId"))
                .WillCascadeOnDelete(true);
        }

        private static void InitializeMessagesTable(DbModelBuilder modelBulBuilder)
        {
            modelBulBuilder.Entity<Message>()
                .HasKey(k => k.Id);

            modelBulBuilder.Entity<Message>()
                .Property(p => p.MessageIdentifier).IsRequired();

            modelBulBuilder.Entity<Message>()
                .Property(p => p.MessageText).IsRequired();
        }
    }
}