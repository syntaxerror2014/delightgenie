﻿using System.Data.Entity;
using DelightGenie.Models.DbModels.AccountModels;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DelightGenie.DAL.DbContexts
{
    public class AccountsContextInitializer
    {
        public static void InitializeModels(DbModelBuilder modelBuilder)
        {
            InitializeUserAccountsTablePart(modelBuilder);
            InitializeEmployeesTablePart(modelBuilder);
            InitializeAdminsTablePart(modelBuilder);

            InitializeGroupsTable(modelBuilder);
            InitializeSessionsTable(modelBuilder);
        }

        private static void InitializeUserAccountsTablePart(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserAccount>()
                .HasKey(k => k.Id);

            modelBuilder.Entity<UserAccount>()
                .Property(p => p.UserName).IsRequired();

            modelBuilder.Entity<UserAccount>()
                .Property(p => p.Email).IsOptional();

            // Map foreign key to the Sessions table
            modelBuilder.Entity<UserAccount>()
                .HasOptional(u => u.Session)
                .WithOptionalPrincipal(u => u.UserAccount)
                .Map(m => m.MapKey("UserId"))
                .WillCascadeOnDelete(true);
        }

        private static void InitializeEmployeesTablePart(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>()
                .Property(p => p.FirstName).IsOptional();

            modelBuilder.Entity<Employee>()
                .Property(p => p.LastName).IsOptional();

            modelBuilder.Entity<Employee>()
                .Property(p => p.WelcomeMessage).IsOptional();

            modelBuilder.Entity<Employee>()
                .Property(p => p.Points).IsOptional();

            // Map foreign key to the Groups table
            modelBuilder.Entity<Employee>()
                .HasRequired(e => e.Group)
                .WithMany(e => e.Employees)
                .Map(m => m.MapKey("GroupId"))
                .WillCascadeOnDelete(true);
        }

        private static void InitializeAdminsTablePart(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Admin>()
                .Property(p => p.Password).IsRequired();
        }

        private static void InitializeGroupsTable(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Group>()
                .HasKey(k => k.Id);

            modelBuilder.Entity<Group>()
                .Property(p => p.GroupName).IsRequired();

            modelBuilder.Entity<Group>()
                .Property(p => p.Points).IsRequired();
        }

        private static void InitializeSessionsTable(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Session>()
                .HasKey(k => k.Id);

            modelBuilder.Entity<Session>()
                .Property(p => p.SessionKey).IsRequired();
        }
    }
}