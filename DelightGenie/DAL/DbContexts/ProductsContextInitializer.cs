﻿using System.Data.Entity;
using DelightGenie.Models.DbModels.ProductModels;

namespace DelightGenie.DAL.DbContexts
{
    public class ProductsContextInitializer
    {
        public static void InitializeModels(DbModelBuilder modelBuilder)
        {
            InitializeProductsTable(modelBuilder);
            InitializeCategoryTable(modelBuilder);
        }

        private static void InitializeProductsTable(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>()
                .HasKey(k => k.Id);

            modelBuilder.Entity<Product>()
                .Property(p => p.ProductName).IsRequired();

            modelBuilder.Entity<Product>()
                .Property(p => p.ProductDescription).IsOptional();

            modelBuilder.Entity<Product>()
                .Property(p => p.ProductPrice).IsRequired();

            // Map foreign key to the Categories table
            modelBuilder.Entity<Product>()
                .HasRequired(p => p.Category)
                .WithMany(p => p.Products)
                .Map(m => m.MapKey("CategoryId"))
                .WillCascadeOnDelete(true);
        }

        private static void InitializeCategoryTable(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .HasKey(k => k.Id);

            modelBuilder.Entity<Category>()
                .Property(p => p.CategoryName).IsRequired();
        }
    }
}