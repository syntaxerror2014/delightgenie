﻿using System;
using System.Data.Entity;
using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Models.DbModels.NotificationModels;
using DelightGenie.Special;

namespace DelightGenie.DAL.DbContexts
{
    public class DatabaseContextInitializer : CreateDatabaseIfNotExists<DatabaseContext>
    {
        protected override void Seed(DatabaseContext context)
        {
            base.Seed(context);

            CreateAdminAccount();
            PopulateMessagesTable();
            PopulateGroupsTable();
        }

        private static void PopulateGroupsTable()
        {
            using (var db = new DatabaseContext())
            {
                db.Groups.Add(new Group
                {
                    Id = Guid.NewGuid(),
                    Points = 0,
                    GroupName = Group.DefaultGroupName
                });

                db.SaveChanges();
            }
        }

        private static void CreateAdminAccount()
        {
            using (var db = new DatabaseContext())
            {
                db.UserAccounts.Add(new Admin
                {
                    Id = Guid.NewGuid(),
                    Email = "delightgenie@gmail.com",
                    UserName = "admin",
                    Password = TokenGenerator.EncryptMD5("admin")
                });

                db.SaveChanges();
            }
        }

        private static void PopulateMessagesTable()
        {
            using (var db = new DatabaseContext())
            {
                db.Messages.Add(new Message
                {
                    Id = Guid.NewGuid(),
                    MessageIdentifier = Message.IdentifierPointsAdded,
                    MessageText = "New points have been added to your account!"
                });

                db.Messages.Add(new Message
                {
                    Id = Guid.NewGuid(),
                    MessageIdentifier = Message.IdentifierPointsDeleted,
                    MessageText = "Some points have been removed from your account!"
                });

                db.Messages.Add(new Message
                {
                    Id = Guid.NewGuid(),
                    MessageIdentifier = Message.IdentifierTransactionsDeleted,
                    MessageText = "Some of your votes have been removed by the administrator!"
                });

                db.SaveChanges();
            }
        }
    }
}