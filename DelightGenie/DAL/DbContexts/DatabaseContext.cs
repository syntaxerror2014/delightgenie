﻿using System.Data.Entity;
using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Models.DbModels.HistoryModels;
using DelightGenie.Models.DbModels.NotificationModels;
using DelightGenie.Models.DbModels.ProductModels;

namespace DelightGenie.DAL.DbContexts
{
    public class DatabaseContext : DbContext
    {
        protected internal DatabaseContext()
            : base("DelightGenieConnection")
        {
            Database.SetInitializer(new DatabaseContextInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            AccountsContextInitializer.InitializeModels(modelBuilder);
            ProductsContextInitializer.InitializeModels(modelBuilder);
            NotificationsContextInitializer.InitializeModels(modelBuilder);
            HistoryContextInitializer.InitializeModels(modelBuilder);
        }

        #region User accounts

        public DbSet<UserAccount> UserAccounts { get; set; }

        public DbSet<Group> Groups { get; set; }

        public DbSet<Session> Sessions { get; set; }

        #endregion

        #region Products

        public DbSet<Product> Products { get; set; }

        public DbSet<Category> Categories { get; set; }

        #endregion

        #region Notifications

        public DbSet<Notification> Notifications { get; set; }

        public DbSet<Message> Messages { get; set; }

        #endregion

        #region History

        public DbSet<Transaction> Transactions { get; set; }

        public DbSet<Commit> Commits { get; set; }

        public DbSet<Purchase> Purchases { get; set; }

        #endregion
    }
}