﻿using System;
using DelightGenie.DAL.DbContexts;
using DelightGenie.DAL.Repositories.History;
using DelightGenie.DAL.Repositories.Notifications;
using DelightGenie.DAL.Repositories.Products;
using DelightGenie.DAL.Repositories.UserAccounts;

namespace DelightGenie.DAL
{
    /// <summary>
    ///     The entire Unit Of Work for the database.
    ///     Thsi class is a singleton
    /// </summary>
    public class DalUnitOfWork : IDisposable
    {
        private static DalUnitOfWork _instance;
        private readonly DatabaseContext _dbContext;

        private DalUnitOfWork()
        {
            _dbContext = new DatabaseContext();
        }

        /// <summary>
        ///     Get the available instance for this class
        /// </summary>
        public static DalUnitOfWork Instance
        {
            get
            {
                _disposed = false;
                return _instance ?? (_instance = new DalUnitOfWork());
            }
        }

        #region Fields for the User Accounts module

        private GroupsRepository _groupsRepository;
        private SessionsRepository _sessionsRepository;
        private UserAccountsRepository _userAccountsRepository;

        #endregion

        #region Fields for the Products module

        private CategoriesRepository _categoriesRepository;
        private ProductsRepository _productsRepository;

        #endregion

        #region Fields for the Notifications module

        private MessagesRepository _messagesRepository;
        private NotificationsRepository _notificationsRepository;

        #endregion

        #region Fields for the History module

        private CommitsRepository _commitsRepository;
        private PurchasesRepository _purchasesRepository;
        private TransactionsRepository _transactionsRepository;

        #endregion

        #region Properties for the User Accounts module

        /// <summary>
        ///     Get the repository representing the UserAccounts model
        /// </summary>
        public UserAccountsRepository UserAccountsRepository
        {
            get
            {
                return _userAccountsRepository ?? (_userAccountsRepository = new UserAccountsRepository(_dbContext));
            }
        }

        /// <summary>
        ///     Get the repository representing the Groups model
        /// </summary>
        public GroupsRepository GroupsRepository
        {
            get { return _groupsRepository ?? (_groupsRepository = new GroupsRepository(_dbContext)); }
        }

        /// <summary>
        ///     Get the repository representing the Sessions model
        /// </summary>
        public SessionsRepository SessionsRepository
        {
            get { return _sessionsRepository ?? (_sessionsRepository = new SessionsRepository(_dbContext)); }
        }

        #endregion

        #region Properties for the Products module

        /// <summary>
        ///     Get the repository representing the Categories model
        /// </summary>
        public CategoriesRepository CategoriesRepository
        {
            get { return _categoriesRepository ?? (_categoriesRepository = new CategoriesRepository(_dbContext)); }
        }

        /// <summary>
        ///     Get the repository representing the Products model
        /// </summary>
        public ProductsRepository ProductsRepository
        {
            get { return _productsRepository ?? (_productsRepository = new ProductsRepository(_dbContext)); }
        }

        #endregion

        #region Properties for the Notifications module

        /// <summary>
        ///     Get the repository representing the Messages model
        /// </summary>
        public MessagesRepository MessagesRepository
        {
            get { return _messagesRepository ?? (_messagesRepository = new MessagesRepository(_dbContext)); }
        }

        /// <summary>
        ///     Get the repository representing the Notifications model
        /// </summary>
        public NotificationsRepository NotificationsRepository
        {
            get
            {
                return _notificationsRepository ?? (_notificationsRepository = new NotificationsRepository(_dbContext));
            }
        }

        #endregion

        #region Properties for the History module

        /// <summary>
        ///     Get the repository representing the Commits model
        /// </summary>
        public CommitsRepository CommitsRepository
        {
            get { return _commitsRepository ?? (_commitsRepository = new CommitsRepository(_dbContext)); }
        }

        /// <summary>
        ///     Get the repository representing the Purchases model
        /// </summary>
        public PurchasesRepository PurchasesRepository
        {
            get { return _purchasesRepository ?? (_purchasesRepository = new PurchasesRepository(_dbContext)); }
        }

        /// <summary>
        ///     Get the repository representing the Transactions model
        /// </summary>
        public TransactionsRepository TransactionsRepository
        {
            get
            {
                return _transactionsRepository ?? (_transactionsRepository = new TransactionsRepository(_dbContext));
            }
        }

        #endregion

        #region Disposing logic

        private static bool _disposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed || !disposing) return;

            _dbContext.Dispose();
            _disposed = true;
        }

        #endregion Disposing logic
    }
}