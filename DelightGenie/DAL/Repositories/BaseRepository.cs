﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using DelightGenie.DAL.DbContexts;
using DelightGenie.DAL.Interfaces;
using DelightGenie.Models.DbModels;

namespace DelightGenie.DAL.Repositories
{
    public class BaseRepository<T> : IBaseInterface where T : AbstractModel
    {
        internal readonly DatabaseContext _dbContext;
        internal readonly DbSet<T> _dbSet;

        internal BaseRepository(DatabaseContext context)
        {
            _dbContext = context;
            _dbSet = context.Set<T>();
        }

        #region CRUD logic

        public virtual T Get(Guid id)
        {
            return _dbSet.Find(id);
        }

        public virtual T[] GetAll()
        {
            return _dbSet.ToArray();
        }

        public virtual T Insert(T entity)
        {
            if (entity == null)
                return null;

            if (entity.Id.Equals(Guid.Empty))
                entity.Id = Guid.NewGuid();

            entity = _dbSet.Add(entity);

            return Save() ? entity : null;
        }

        public virtual T Update(T entity)
        {
            if (entity == null)
                return null;

            _dbSet.AddOrUpdate(entity);

            return Save() ? entity : null;
        }

        public virtual bool Delete(Guid id)
        {
            var entity = Get(id);

            return Delete(entity);
        }

        public virtual bool Delete(T entity)
        {
            if (entity == null)
                return false;

            _dbSet.Remove(entity);

            return Save();
        }

        #endregion

        #region Persistence logic

        public bool Save()
        {
            try
            {
                _dbContext.SaveChanges();

                return true;
            }

            catch
            {
                return false;
            }
        }

        public bool SaveAsync()
        {
            try
            {
                _dbContext.SaveChangesAsync();

                return true;
            }

            catch
            {
                return false;
            }
        }

        #endregion
    }
}