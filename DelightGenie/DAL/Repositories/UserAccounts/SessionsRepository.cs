﻿using System;
using System.Linq;
using DelightGenie.DAL.DbContexts;
using DelightGenie.DAL.Interfaces.UserAccounts;
using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Special;

namespace DelightGenie.DAL.Repositories.UserAccounts
{
    public class SessionsRepository : BaseRepository<Session>, ISessionsRepository
    {
        internal SessionsRepository(DatabaseContext context)
            : base(context)
        {
        }

        /// <summary>
        ///     Get the user that has the selected session details
        /// </summary>
        /// <param name="sessionId">The Id of the session</param>
        /// <param name="sessionKey">The Key for the session</param>
        /// <returns>An UserAccount instance or null if there is no session for the user</returns>
        public UserAccount GetUser(Guid sessionId, string sessionKey)
        {
            try
            {
                return _dbSet.First(s => s.Id == sessionId && s.SessionKey == sessionKey).UserAccount;
            }

            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Open a session for a certain user
        /// </summary>
        /// <param name="userName">The nickname of the user who wants to log in</param>
        /// <returns>A Session instance or a null value if it doesn't exist</returns>
        public Session Login(string userName)
        {
            try
            {
                var session = new Session
                {
                    UserAccount = _dbContext.UserAccounts.First(u => u.UserName == userName),
                    SessionKey = TokenGenerator.EncryptMD5(DateTime.Now.ToFileTime().ToString())
                };

                return Insert(session);
            }

            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Close a session for a certain user
        /// </summary>
        /// <param name="userName">The nickname of the user who wants to log out</param>
        /// <returns>A bool value indicating the success of the action</returns>
        public bool Logout(string userName)
        {
            var session = _dbSet.First(t => t.UserAccount.UserName == userName);

            return Delete(session);
        }
    }
}