﻿using System;
using System.Linq;
using DelightGenie.DAL.DbContexts;
using DelightGenie.DAL.Interfaces.UserAccounts;
using DelightGenie.Models.DbModels.AccountModels;
using WebGrease.Css.Extensions;

namespace DelightGenie.DAL.Repositories.UserAccounts
{
    public class GroupsRepository : BaseRepository<Group>, IGroupsRepository
    {
        internal GroupsRepository(DatabaseContext context)
            : base(context)
        {
        }

        /// <summary>
        ///     Reset all the points for all the members of a certaing group
        /// </summary>
        /// <param name="groupId">The Id of the group</param>
        public bool ResetGroupPoints(Guid groupId)
        {
            try
            {
                _dbSet.Find(groupId).Employees.ForEach(p => p.Points = p.Group.Points);

                return true;
            }

            catch
            {
                return false;
            }
        }

        /// <summary>
        ///     Get all the employees that are part of a certain group
        /// </summary>
        /// <param name="groupId">The Guid of the group</param>
        /// <returns></returns>
        public Employee[] GetUsersForGroup(Guid groupId)
        {
            try
            {
                // ReSharper disable once PossibleNullReferenceException
                return Get(groupId).Employees.ToArray();
            }

            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Get a certain group
        /// </summary>
        /// <param name="groupName">The Name of the group</param>
        /// <returns>A Group entity</returns>
        public Group GetGroup(string groupName)
        {
            try
            {
                return _dbSet.FirstOrDefault(g => g.GroupName == groupName);
            }

            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Add a certain user to a certain group
        /// </summary>
        /// <param name="user">The UserAccount to add to the group</param>
        /// <param name="groupId">The Id of the group where to add the user</param>
        public bool AddUserToGroup(UserAccount user, Guid groupId)
        {
            try
            {
                Group group = _dbSet.Find(groupId);
                group.Employees.Add(user as Employee);

                return Update(group) != null;
            }

            catch
            {
                return false;
            }
        }

        /// <summary>
        ///     Add a range of users to a certaing group
        /// </summary>
        /// <param name="users">The UserAccount collection to add to the group</param>
        /// <param name="groupId">The Id of the group for which to do the process</param>
        public bool AddUsersToGroup(UserAccount[] users, Guid groupId)
        {
            try
            {
                Group group = Get(groupId);

                users.ForEach(t => group.Employees.Add(t as Employee));

                return Update(group) != null;
            }

            catch
            {
                return false;
            }
        }

        /// <summary>
        ///     Insert a new group into the database
        /// </summary>
        /// <param name="groupName">The name of the group</param>
        /// <param name="points">The default points that this group has</param>
        public Group Insert(string groupName, int points)
        {
            var group = new Group
            {
                GroupName = groupName,
                Points = points
            };

            return Insert(group);
        }

        /// <summary>
        /// Verify if a group with a certain name already exists
        /// </summary>
        /// <param name="groupName">The name of the group to search for</param>
        /// <returns></returns>
        public bool VerifyExists(string groupName)
        {
            return _dbSet.Any(g => g.GroupName == groupName);
        }
    }
}