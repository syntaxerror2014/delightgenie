﻿using System;
using System.Linq;
using DelightGenie.DAL.DbContexts;
using DelightGenie.DAL.Interfaces.UserAccounts;
using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Models.DbModels.HistoryModels;
using DelightGenie.Special;

namespace DelightGenie.DAL.Repositories.UserAccounts
{
    public class UserAccountsRepository : BaseRepository<UserAccount>, IUserAccountsRepository
    {
        internal UserAccountsRepository(DatabaseContext context)
            : base(context)
        {
        }

        /// <summary>
        ///     Get the Admin account for the website
        /// </summary>
        /// <returns>An Admin entity</returns>
        public Admin GetAdmin()
        {
            try
            {
                return _dbSet.OfType<Admin>().FirstOrDefault();
            }

            catch
            {
                return null;
            }
        }

        public Employee[] GetAllEmployees()
        {
            try
            {
                return _dbSet.OfType<Employee>().ToArray();
            }

            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Get an employee that this site has
        /// </summary>
        /// <param name="userName">The username of the employee</param>
        /// <returns>An Employee entity</returns>
        public Employee GetEmployee(string userName)
        {
            try
            {
                return _dbSet.OfType<Employee>().FirstOrDefault(u => u.UserName == userName);
            }

            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Get all the transactions that an employee has for a certain commit
        /// </summary>
        /// <param name="employeeId">The Id of the employee</param>
        /// <param name="commit">The Commit for which to get the list</param>
        /// <returns>An array of Transaction entities</returns>
        public Transaction[] GetTransactionsForEmployee(Guid employeeId, Commit commit)
        {
            try
            {
                // ReSharper disable once PossibleNullReferenceException
                return (_dbSet.Find(employeeId) as Employee).Transactions
                    .Where(t => t.TransactionDate > commit.CommitDate).ToArray();
            }

            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Verify if a certain user exists
        /// </summary>
        /// <param name="userName">The username for which to check</param>
        public bool VerifyExists(string userName)
        {
            try
            {
                return _dbSet.Count(u => u.UserName == userName) > 0;
            }

            catch
            {
                return false;
            }
        }

        /// <summary>
        ///     Verify the password for an admin account
        /// </summary>
        /// <param name="userName">The username of the admin</param>
        /// <param name="password">The password for the admin</param>
        /// <returns></returns>
        public bool VerifyPassword(string userName, string password)
        {
            try
            {
                var admin = _dbSet.FirstOrDefault(u => u.UserName == userName);

                if (!(admin is Admin))
                    return false;

                return (admin as Admin).Password == TokenGenerator.EncryptMD5(password);
            }

            catch
            {
                return false;
            }
        }

        /// <summary>
        ///     Substract a certain number of points from a user
        /// </summary>
        /// <param name="userId">The Id of the user from who to extract the points</param>
        /// <param name="points">The number of points to substract</param>
        public Employee SubstractPointsForUser(Guid userId, int points)
        {
            var user = Get(userId) as Employee;

            // ReSharper disable once PossibleNullReferenceException
            if (user.Points < points) return null;

            user.Points -= points;

            return Update(user) as Employee;
        }

        /// <summary>
        ///     Set the number of points that a user should have
        /// </summary>
        /// <param name="userId">The Id of the user</param>
        /// <param name="points">The number of points to set</param>
        public Employee SetPointsForUser(Guid userId, int points)
        {
            var user = Get(userId) as Employee;

            // ReSharper disable once PossibleNullReferenceException
            user.Points = points;

            return Update(user) as Employee;
        }

        /// <summary>
        ///     Delete a certain user account.
        /// </summary>
        /// <param name="userName">The username of the account to delete</param>
        public bool Delete(string userName)
        {
            if (userName == "admin")
                return false;

            var user = _dbSet.FirstOrDefault(u => u.UserName == userName);

            return Delete(user);
        }
    }
}