﻿using DelightGenie.DAL.DbContexts;
using DelightGenie.DAL.Interfaces.History;
using DelightGenie.Models.DbModels.HistoryModels;
using DelightGenie.Models.DbModels.ProductModels;

namespace DelightGenie.DAL.Repositories.History
{
    public class PurchasesRepository : BaseRepository<Purchase>, IPurchasesRepository
    {
        internal PurchasesRepository(DatabaseContext context)
            : base(context)
        {
        }

        /// <summary>
        ///     Insert a new purchase into the database
        /// </summary>
        /// <param name="numberOfItems">The number of items for the purchase</param>
        /// <param name="itemPrice">The price for a unit of the item</param>
        /// <param name="points">The number of points that the product has received in total</param>
        /// <param name="commit">The commit to which the purchase is binded</param>
        /// <param name="product">The product that the purchase represents</param>
        public Purchase Insert(int numberOfItems, float itemPrice, int points, Commit commit,
            Product product)
        {
            var purchase = new Purchase
            {
                Product = product,
                Commit = commit,
                Points = points,
                ItemPrice = itemPrice,
                NumberOfItems = numberOfItems
            };

            return Insert(purchase);
        }
    }
}