﻿using System;
using System.Linq;
using DelightGenie.DAL.DbContexts;
using DelightGenie.DAL.Interfaces.History;
using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Models.DbModels.HistoryModels;
using DelightGenie.Models.DbModels.ProductModels;

namespace DelightGenie.DAL.Repositories.History
{
    public class TransactionsRepository : BaseRepository<Transaction>, ITransactionsRepository
    {
        internal TransactionsRepository(DatabaseContext context)
            : base(context)
        {
        }

        /// <summary>
        ///     Get all the transactions that an employee has made after the last commit
        /// </summary>
        /// <param name="employee">The employee for which to get the transaction</param>
        /// <returns></returns>
        public Transaction[] GetUncommitedTransactionsForEmployee(Employee employee)
        {
            try
            {
                var commit = DalUnitOfWork.Instance.CommitsRepository.GetLastCommit();

                if (commit == null || commit.Id.Equals(Guid.Empty))
                    return GetAll();

                var transactions =
                    _dbSet.Where(t => t.Employee.Id.Equals(employee.Id) && t.TransactionDate.Value > commit.CommitDate.Value);

                return transactions.ToArray();
            }

            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Get all the transactions that a commit has
        /// </summary>
        /// <param name="commit">The Commit entity</param>
        /// <param name="newTransactions"></param>
        /// <returns>An array of Transaction entities</returns>
        public Transaction[] GetTransactionsForCommit(Commit commit,
            bool newTransactions = false)
        {
            try
            {
                // Fetch the transactions
                Transaction[] transactions;

                // If the commit is null or doesn't exist, fetch transactions that have been made after the last commit
                if (commit == null || commit.Id.Equals(Guid.Empty))
                {
                    commit = DalUnitOfWork.Instance.CommitsRepository.GetLastCommit();

                    transactions = commit != null ?
                                                  _dbSet.Where(t => t.TransactionDate.Value > commit.CommitDate.Value).ToArray()
                                                  : GetAll();
                }

                // Get all transactions between the entered commit and te previous one
                else
                {
                    var previousCommit = _dbContext.Commits.OrderByDescending(c => c.CommitDate.Value)
                                                            .FirstOrDefault(c => c.CommitDate.Value < commit.CommitDate.Value);
                    
                    // If there is no previous commit, then get all the transactions between the first day of production for the app,
                    // and the date this commit was made
                    if (previousCommit == null)
                        transactions = _dbSet.Where(t => t.TransactionDate.Value < commit.CommitDate.Value).ToArray();

                    else
                    {
                        transactions = _dbSet.Where(t => t.TransactionDate.Value > previousCommit.CommitDate.Value &&
                                                        t.TransactionDate.Value < commit.CommitDate.Value)
                                                        .ToArray();
                    }
                }

                // Group the transactions into distinct entities contaning the overall score of each product
                var distinctProducts = transactions.Select(t => t.Product).Distinct().ToArray();

                var finalTransactions = new Transaction[distinctProducts.Count()];
                for (var i = 0; i < distinctProducts.Count(); i++)
                {
                    finalTransactions[i] = new Transaction
                    {
                        Id = Guid.NewGuid(),
                        Product = distinctProducts[i],
                        Points = transactions.Where(t => t.Product.Id.Equals(distinctProducts[i].Id))
                                             .Sum(t => t.Points)
                    };
                }

                    return finalTransactions;
            }

            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Insert a new transaction into the database
        /// </summary>
        /// <param name="points">The number of points that the transaction has</param>
        /// <param name="product">The Product for which the transaction is made</param>
        /// <param name="employee">The employee which makes the transaction</param>
        /// <returns></returns>
        public Transaction Insert(int points, Product product,
            Employee employee)
        {
            var transaction = new Transaction
            {
                Employee = employee,
                Product = product,
                Points = points,
                TransactionDate = DateTime.Now
            };

            return Insert(transaction);
        }
    }
}