﻿using System;
using System.Linq;
using DelightGenie.DAL.DbContexts;
using DelightGenie.DAL.Interfaces.History;
using DelightGenie.Models.DbModels.HistoryModels;
using WebGrease.Css.Extensions;

namespace DelightGenie.DAL.Repositories.History
{
    public class CommitsRepository : BaseRepository<Commit>, ICommitsRepository
    {
        internal CommitsRepository(DatabaseContext context)
            : base(context)
        {
        }

        /// <summary>
        ///     Get the last commit from the database
        /// </summary>
        /// <returns>A Commit entity</returns>
        public Commit GetLastCommit()
        {
            try
            {
                return _dbSet.OrderByDescending(c => c.CommitDate).FirstOrDefault();
            }

            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Get a list of purchases that a certain commit has
        /// </summary>
        /// <param name="commitId">The Id of the commit</param>
        /// <returns>An array of Purchase entities</returns>
        public Purchase[] GetPurchasesForCommit(Guid commitId)
        {
            try
            {
                return Get(commitId).Purchases.ToArray();
            }

            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Add a new purchase to a commit
        /// </summary>
        /// <param name="purchase">The Purchase entity to add</param>
        /// <param name="commitId">The Id of the commit where to add</param>
        public Commit AddPurchase(Purchase purchase, Guid commitId)
        {
            var commit = Get(commitId);

            commit.Purchases.Add(purchase);

            return Update(commit);
        }

        /// <summary>
        ///     Add a range of purchases to a commit
        /// </summary>
        /// <param name="purchases">The array of Purchase entities to add</param>
        /// <param name="commitId">The Id of the commit where to add</param>
        public Commit AddPurchases(Purchase[] purchases, Guid commitId)
        {
            Commit commit = Get(commitId);

            purchases.ForEach(p => commit.Purchases.Add(p));

            return Update(commit);
        }

        /// <summary>
        ///     Insert a new commit into the database
        /// </summary>
        /// <param name="budget">The budget available for that commit</param>
        public Commit Insert(float budget)
        {
            var commit = new Commit
            {
                CommitDate = DateTime.Now,
                Budget = budget
            };

            return Insert(commit);
        }
    }
}