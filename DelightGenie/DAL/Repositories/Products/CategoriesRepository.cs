﻿using System;
using System.Linq;
using DelightGenie.DAL.DbContexts;
using DelightGenie.DAL.Interfaces.Products;
using DelightGenie.Models.DbModels.HistoryModels;
using DelightGenie.Models.DbModels.ProductModels;
using WebGrease.Css.Extensions;

namespace DelightGenie.DAL.Repositories.Products
{
    public class CategoriesRepository : BaseRepository<Category>, ICategoriesRepository
    {
        internal CategoriesRepository(DatabaseContext context)
            : base(context)
        {
        }

        /// <summary>
        ///     Get a certain category
        /// </summary>
        /// <param name="categoryName">The Id of the category to fetch</param>
        /// <returns>A Category entity</returns>
        public Category GetCategory(string categoryName)
        {
            try
            {
                return _dbSet.FirstOrDefault(c => c.CategoryName == categoryName);
            }

            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Get a list of products that care located in a certain category
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="commit">
        ///     The commit for which to get the products.
        ///     If the commit is null, then all the products in the category are returned
        /// </param>
        /// <returns></returns>
        public Product[] GetProducts(Guid categoryId, Commit commit = null)
        {
            try
            {
                Category category = Get(categoryId);

                return commit != null
                    ? category.Products
                        .OrderByDescending(p => p.Transactions
                            .Where(t => t.TransactionDate > commit.CommitDate)
                            .Sum(t => t.Points))
                        .ToArray()
                    : category.Products.ToArray();
            }

            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Add a product to a certain category
        /// </summary>
        /// <param name="product">The product for which to allocate a category</param>
        /// <param name="categoryId">The Id of the category where to place the product</param>
        public Category AddProductToCategory(Product product, Guid categoryId)
        {
            product.Category.Products.Remove(product);

            Category category = Get(categoryId);
            category.Products.Add(product);

            return Update(category);
        }

        /// <summary>
        ///     Add a range of products to a certain category
        /// </summary>
        /// <param name="products">An array of Product entities to be added to the category</param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public Category AddProductsToCategory(Product[] products, Guid categoryId)
        {
            Category category = Get(categoryId);

            products.ForEach(t => t.Category.Products.Remove(t));
            products.ForEach(t => category.Products.Add(t));

            return Update(category);
        }

        /// <summary>
        ///     Insert a new category into the database
        /// </summary>
        /// <param name="categoryName">The name of the category</param>
        public Category Insert(string categoryName)
        {
            var category = new Category
            {
                CategoryName = categoryName
            };

            return Insert(category);
        }
    }
}