﻿using System;
using System.Linq;
using DelightGenie.DAL.DbContexts;
using DelightGenie.DAL.Interfaces.Products;
using DelightGenie.Models.DbModels.HistoryModels;
using DelightGenie.Models.DbModels.ProductModels;

namespace DelightGenie.DAL.Repositories.Products
{
    public class ProductsRepository : BaseRepository<Product>, IProductsRepository
    {
        internal ProductsRepository(DatabaseContext context) : base(context)
        {
        }


        /// <summary>
        ///     Get a certain product
        /// </summary>
        /// <param name="productName">The Id of the product to get</param>
        /// <returns>A Product entity</returns>
        public Product GetProduct(string productName)
        {
            try
            {
                return _dbSet.FirstOrDefault(p => p.ProductName == productName);
            }

            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Get a list of transactions that have been made for a certain product
        /// </summary>
        /// <param name="productId">The Id of the product</param>
        /// <returns>An array of Transaction entities</returns>
        public Transaction[] GetTransactionsForProduct(Guid productId)
        {
            try
            {
                return Get(productId).Transactions.ToArray();
            }

            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Get a list of purchases that have been made for a certain product
        /// </summary>
        /// <param name="productId">The Id of the product</param>
        /// <returns>An array of Purchase entities</returns>
        public Purchase[] GetPurchasesForProduct(Guid productId)
        {
            try
            {
                return Get(productId).Purchases.ToArray();
            }

            catch
            {
                return null;
            }
        }
    }
}