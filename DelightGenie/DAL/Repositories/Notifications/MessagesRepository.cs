﻿using System.Linq;
using DelightGenie.DAL.DbContexts;
using DelightGenie.DAL.Interfaces.Notifications;
using DelightGenie.Models.DbModels.NotificationModels;

namespace DelightGenie.DAL.Repositories.Notifications
{
    public class MessagesRepository : BaseRepository<Message>, IMessagesRepository
    {
        internal MessagesRepository(DatabaseContext context)
            : base(context)
        {
        }

        /// <summary>
        ///     Get the text of a message based on a special identifier
        /// </summary>
        /// <param name="messageIdentifier">The identifier (int) representing the message type</param>
        /// <returns>A string value</returns>
        public Message GetMessage(int messageIdentifier)
        {
            try
            {
                return _dbSet.FirstOrDefault(m => m.MessageIdentifier == messageIdentifier);
            }

            catch
            {
                return null;
            }
        }
    }
}