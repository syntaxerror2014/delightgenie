﻿using System;
using System.Linq;
using DelightGenie.DAL.DbContexts;
using DelightGenie.DAL.Interfaces.Notifications;
using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Models.DbModels.NotificationModels;

namespace DelightGenie.DAL.Repositories.Notifications
{
    public class NotificationsRepository : BaseRepository<Notification>, INotificationsRepository
    {
        internal NotificationsRepository(DatabaseContext context)
            : base(context)
        {
        }

        #region Account creation notifications

        /// <summary>
        ///     Get a certain Account creation notification
        /// </summary>
        /// <param name="accountCreationNotificationId">The Id of the entity to retrieve</param>
        /// <returns>An AccountCreationNotification entity</returns>
        public AccountCreateNotification GetAccountCreateNotification(
            Guid accountCreationNotificationId)
        {
            return Get(accountCreationNotificationId) as AccountCreateNotification;
        }

        /// <summary>
        ///     Get all the Account creation notification that are available from the database
        /// </summary>
        /// <returns>An array of AccountCreateNotification entities</returns>
        public AccountCreateNotification[] GetAllAccountCreateNotifications()
        {
            return _dbSet.OfType<AccountCreateNotification>().ToArray();
        }

        /// <summary>
        ///     Insert a new Account creation notification into the database
        /// </summary>
        /// <param name="firstName">The first name of the person</param>
        /// <param name="lastName">The last name of the person</param>
        /// <param name="email">The email of the person</param>
        public AccountCreateNotification InsertAccountCreateNotification(string firstName,
            string lastName, string email)
        {
            var accountCreateNotification = new AccountCreateNotification
            {
                Email = email,
                FirstName = firstName,
                LastName = lastName,
                NotificationDate = DateTime.Now
            };

            return Insert(accountCreateNotification) as AccountCreateNotification;
        }

        #endregion

        #region Product creation notifications

        /// <summary>
        ///     Get a certain Product creation notification
        /// </summary>
        /// <param name="productCreationNotificationId">The Id of the notification entity</param>
        /// <returns>A ProductCreateNotification entity</returns>
        public ProductCreateNotification GetProductCreateNotification(
            Guid productCreationNotificationId)
        {
            return Get(productCreationNotificationId) as ProductCreateNotification;
        }

        /// <summary>
        ///     Get a list with all the Product creation notifications availabe in the database
        /// </summary>
        /// <returns>An array of ProductCreateNotification entities</returns>
        public ProductCreateNotification[] GetAllProductCreateNotifications()
        {
            return _dbSet.OfType<ProductCreateNotification>().ToArray();
        }

        /// <summary>
        ///     Insert a new Product creation notification entity into the database
        /// </summary>
        /// <param name="productName">The name of the product</param>
        /// <param name="description">The description left by the user</param>
        /// <param name="employee">The user that created the notification</param>
        public ProductCreateNotification InsertProductCreateNotification(string productName,
            string description, Employee employee)
        {
            var productCreateNotification = new ProductCreateNotification
            {
                Employee = employee,
                Description = description,
                ProductName = productName,
                NotificationDate = DateTime.Now
            };

            return Insert(productCreateNotification) as ProductCreateNotification;
        }

        #endregion

        #region Points notifications

        /// <summary>
        ///     Get a certain Points notification
        /// </summary>
        /// <param name="pointsNotificationId">The Id of the notification entity</param>
        /// <returns>A PointsNotification entity</returns>
        public PointsNotification GetPointsNotification(Guid pointsNotificationId)
        {
            return _dbSet.Find(pointsNotificationId) as PointsNotification;
        }

        /// <summary>
        ///     Get all the existent Points notifications for a certain employee
        /// </summary>
        /// <param name="employeeId">The Id of the employee for which to fetch the data</param>
        /// <returns>An array of PointsNotification entities</returns>
        public PointsNotification[] GetAllPointsNotifications(Guid employeeId)
        {
            try
            {
                return _dbSet.OfType<PointsNotification>()
                    .Where(t => t.Employee.Id == employeeId)
                    .ToArray();
            }

            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Add a new Points notification to the database
        /// </summary>
        /// <param name="message">The Message entity to deliver with the notification</param>
        /// <param name="employee">The employee that will receive the message</param>
        public PointsNotification InsertPointsNotification(Message message,
            Employee employee)
        {
            var pointsNotification = new PointsNotification
            {
                Employee = employee,
                Message = message,
                NotificationDate = DateTime.Now
            };

            return Insert(pointsNotification) as PointsNotification;
        }

        #endregion
    }
}