﻿using DelightGenie.Models.DbModels.NotificationModels;

namespace DelightGenie.DAL.Interfaces.Notifications
{
    internal interface IMessagesRepository
    {
        Message GetMessage(int messageIdentifier);
    }
}