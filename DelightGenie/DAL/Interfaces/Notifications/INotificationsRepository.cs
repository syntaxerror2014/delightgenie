﻿using System;
using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Models.DbModels.NotificationModels;

namespace DelightGenie.DAL.Interfaces.Notifications
{
    internal interface INotificationsRepository
    {
        #region AccountCreateNotifications

        AccountCreateNotification GetAccountCreateNotification(Guid accountCreationNotificationId);

        AccountCreateNotification[] GetAllAccountCreateNotifications();

        AccountCreateNotification InsertAccountCreateNotification(string firstName, string lastName,
            string email);

        #endregion

        #region ProductCreateNotifications

        ProductCreateNotification GetProductCreateNotification(Guid productCreationNotificationId);

        ProductCreateNotification[] GetAllProductCreateNotifications();

        ProductCreateNotification InsertProductCreateNotification(string productName,
            string description, Employee employee);

        #endregion

        #region PointsCreateNotifications

        PointsNotification GetPointsNotification(Guid pointsNotificationId);

        PointsNotification[] GetAllPointsNotifications(Guid employeeId);

        PointsNotification InsertPointsNotification(Message message,
            Employee employee);

        #endregion
    }
}