﻿using System;
using DelightGenie.Models.DbModels.AccountModels;

namespace DelightGenie.DAL.Interfaces.UserAccounts
{
    internal interface ISessionsRepository
    {
        UserAccount GetUser(Guid sessionId, string sessionKey);

        Session Login(string userName);

        bool Logout(string userName);
    }
}