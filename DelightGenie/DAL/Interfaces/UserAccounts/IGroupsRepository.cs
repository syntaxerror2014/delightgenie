﻿using System;
using DelightGenie.Models.DbModels.AccountModels;

namespace DelightGenie.DAL.Interfaces.UserAccounts
{
    internal interface IGroupsRepository
    {
        bool ResetGroupPoints(Guid groupId);

        Employee[] GetUsersForGroup(Guid groupId);

        Group GetGroup(string groupName);

        bool AddUserToGroup(UserAccount user, Guid groupId);

        bool AddUsersToGroup(UserAccount[] users, Guid groupId);

        Group Insert(string groupName, int points);

        bool VerifyExists(string groupName);
    }
}