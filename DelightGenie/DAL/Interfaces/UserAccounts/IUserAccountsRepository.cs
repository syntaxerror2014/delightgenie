﻿using System;
using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Models.DbModels.HistoryModels;

namespace DelightGenie.DAL.Interfaces.UserAccounts
{
    internal interface IUserAccountsRepository
    {
        Admin GetAdmin();

        Employee[] GetAllEmployees();

        Employee GetEmployee(string userName);

        Transaction[] GetTransactionsForEmployee(Guid employeeId, Commit commit);

        bool VerifyExists(string userName);

        bool VerifyPassword(string userName, string password);

        Employee SubstractPointsForUser(Guid userId, int points);

        Employee SetPointsForUser(Guid userId, int points);

        bool Delete(string userName);
    }
}
