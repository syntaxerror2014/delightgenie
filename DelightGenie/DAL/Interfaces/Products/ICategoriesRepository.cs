﻿using System;
using DelightGenie.Models.DbModels.HistoryModels;
using DelightGenie.Models.DbModels.ProductModels;

namespace DelightGenie.DAL.Interfaces.Products
{
    internal interface ICategoriesRepository
    {
        Category GetCategory(string categoryName);

        Product[] GetProducts(Guid categoryId, Commit commit = null);

        Category AddProductToCategory(Product product, Guid categoryId);

        Category AddProductsToCategory(Product[] products, Guid categoryId);

        Category Insert(string categoryName);
    }
}