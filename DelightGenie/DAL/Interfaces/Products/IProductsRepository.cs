﻿using System;
using DelightGenie.Models.DbModels.HistoryModels;
using DelightGenie.Models.DbModels.ProductModels;

namespace DelightGenie.DAL.Interfaces.Products
{
    internal interface IProductsRepository
    {
        Product GetProduct(string productName);

        Transaction[] GetTransactionsForProduct(Guid productId);

        Purchase[] GetPurchasesForProduct(Guid productId);
    }
}