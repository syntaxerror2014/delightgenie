﻿namespace DelightGenie.DAL.Interfaces
{
    internal interface IBaseInterface
    {
        bool Save();

        bool SaveAsync();
    }
}