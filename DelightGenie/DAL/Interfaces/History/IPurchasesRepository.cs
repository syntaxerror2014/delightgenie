﻿using DelightGenie.Models.DbModels.HistoryModels;
using DelightGenie.Models.DbModels.ProductModels;

namespace DelightGenie.DAL.Interfaces.History
{
    internal interface IPurchasesRepository
    {
        Purchase Insert(int numberOfItems, float itemPrice, int points, Commit commit,
            Product product);
    }
}