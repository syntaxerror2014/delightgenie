﻿using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Models.DbModels.HistoryModels;
using DelightGenie.Models.DbModels.ProductModels;

namespace DelightGenie.DAL.Interfaces.History
{
    internal interface ITransactionsRepository
    {
        Transaction[] GetUncommitedTransactionsForEmployee(Employee employee);

        Transaction[] GetTransactionsForCommit(Commit commit, bool newTransactions = false);

        Transaction Insert(int points, Product product, Employee employee);
    }
}