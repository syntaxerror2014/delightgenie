﻿using System;
using DelightGenie.Models.DbModels.HistoryModels;

namespace DelightGenie.DAL.Interfaces.History
{
    internal interface ICommitsRepository
    {
        Commit GetLastCommit();

        Purchase[] GetPurchasesForCommit(Guid commitId);

        Commit AddPurchase(Purchase purchase, Guid commitId);

        Commit AddPurchases(Purchase[] purchases, Guid commitId);

        Commit Insert(float budget);
    }
}