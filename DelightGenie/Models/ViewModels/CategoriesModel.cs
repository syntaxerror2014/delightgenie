﻿namespace DelightGenie.Models.ViewModels
{
    public class CategoriesModel
    {
        public string CategoryName { get; set; }
        public string CategoryId { get; set; }
    }
}