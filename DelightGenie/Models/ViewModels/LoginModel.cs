﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DelightGenie.Models.ViewModels
{
    public class LoginModel
    {
        [Required]
        [DataType(DataType.Text)]
        [StringLength(20, MinimumLength = 4)]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email address")]
        public string EmailAddress { get; set; }

        [DataType(DataType.Text)]
        [DisplayName("First name")]
        public string FirstName { get; set; }

        [DataType(DataType.Text)]
        [DisplayName("Last name")]
        public string LastName { get; set; }
    }
}