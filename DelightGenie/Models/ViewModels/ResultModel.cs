﻿namespace DelightGenie.Models.ViewModels
{
    public class ResultModel
    {
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public float ProductPrice { get; set; }

        public int TotalScore { get; set; }

        public float Percentage { get; set; }

        public int NrItems { get; set; }

        public float AllPrice { get; set; }
    }
}