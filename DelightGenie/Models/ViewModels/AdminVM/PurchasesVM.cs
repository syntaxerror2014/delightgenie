﻿using DelightGenie.Models.DbModels.HistoryModels;

namespace DelightGenie.Models.ViewModels.AdminVM
{
    public class PurchasesVM
    {
        public bool ViewExistent = false;
        public Purchase[] Purchases { get; set; }

        public Transaction[] Transactions { get; set; }

        public ResultModel[] Results { get; set; }

        public int TotalAllScore { get; set; }

        public float TotalAllPrice { get; set; }

        public string CommitID { get; set; }
    }
}