﻿using DelightGenie.Models.DbModels.AccountModels;

namespace DelightGenie.Models.ViewModels.AdminVM
{
    public class GroupsVM
    {
        public Group[] Groups { get; set; }
    }
}