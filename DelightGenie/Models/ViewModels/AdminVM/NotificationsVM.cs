﻿using DelightGenie.Models.DbModels.NotificationModels;

namespace DelightGenie.Models.ViewModels.AdminVM
{
    public class NotificationsVM
    {
        public ProductCreateNotification[] notifications { get; set; }
    }
}