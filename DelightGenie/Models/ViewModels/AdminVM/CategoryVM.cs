﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DelightGenie.Models.ViewModels.AdminVM
{
    public class CategoryVM
    {
        [Required]
        [DataType(DataType.Text)]
        [DisplayName("Category Name")]
        public string Name { get; set; }

        public string Id { get; set; }
    }
}