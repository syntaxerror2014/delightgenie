﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DelightGenie.Models.ViewModels.AdminVM
{
    public class SettingsVM
    {
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [DisplayName("New Password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [DisplayName("New Password Again")]
        public string NewPasswordAgain { get; set; }

        [DataType(DataType.Text)]
        [DisplayName("LDAP server address")]
        public string LDAPAddress { get; set; }

        [DataType(DataType.Text)]
        [DisplayName("LDAP string")]
        public string LDAPString { get; set; }
    }
}