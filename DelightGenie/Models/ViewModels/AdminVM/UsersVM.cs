﻿using DelightGenie.Models.DbModels.AccountModels;

namespace DelightGenie.Models.ViewModels.AdminVM
{
    public class UsersVM
    {
        public Employee[] Employees { get; set; }
    }
}