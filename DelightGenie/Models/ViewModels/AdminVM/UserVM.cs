﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using DelightGenie.Models.DbModels.AccountModels;

namespace DelightGenie.Models.ViewModels.AdminVM
{
    public class UserVM
    {
        public Group[] Groups { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [DisplayName("Username")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email address")]
        public string EmailAddress { get; set; }

        [DataType(DataType.Text)]
        [DisplayName("First name")]
        public string FirstName { get; set; }

        [DataType(DataType.Text)]
        [DisplayName("Last name")]
        public string LastName { get; set; }

        [DataType(DataType.Text)]
        [DisplayName("Points")]
        public int Points { get; set; }

        public string Group { get; set; }

        public string OldUserName { get; set; }
    }
}