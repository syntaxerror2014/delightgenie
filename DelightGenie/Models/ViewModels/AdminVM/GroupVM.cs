﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DelightGenie.Models.ViewModels.AdminVM
{
    public class GroupVM
    {
        [Required]
        [DataType(DataType.Text)]
        [DisplayName("Group Name")]
        public string Name { get; set; }

        [DataType(DataType.Text)]
        [DisplayName("Group Points")]
        public string Points { get; set; }


        public string Id { get; set; }
    }
}