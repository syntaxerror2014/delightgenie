﻿using DelightGenie.Models.DbModels.ProductModels;

namespace DelightGenie.Models.ViewModels.AdminVM
{
    public class CategoriesVM
    {
        public Category[] Categories { get; set; }
    }
}