﻿using DelightGenie.Models.DbModels.ProductModels;

namespace DelightGenie.Models.ViewModels.AdminVM
{
    public class ProductsVM
    {
        public Product[] Products { get; set; }

        public Category[] Categories { get; set; }
    }
}