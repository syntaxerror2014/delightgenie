﻿using DelightGenie.Models.DbModels.HistoryModels;

namespace DelightGenie.Models.ViewModels.AdminVM
{
    public class CommitsVM
    {
        public Commit[] Commits { get; set; }
    }
}