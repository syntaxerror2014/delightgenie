﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using DelightGenie.Models.DbModels.ProductModels;

namespace DelightGenie.Models.ViewModels.AdminVM
{
    public class ProductVM
    {
        public Category[] Categories { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [DisplayName("Product Name")]
        public string Name { get; set; }

        [DataType(DataType.Text)]
        [DisplayName("Product Description")]
        public string Description { get; set; }

        [DataType(DataType.Text)]
        [DisplayName("Product Price")]
        public float Price { get; set; }

        public string Id { get; set; }

        public string Category { get; set; }

        public string NotificationId { get; set; }
    }
}