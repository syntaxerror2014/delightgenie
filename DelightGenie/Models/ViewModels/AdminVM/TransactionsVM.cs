﻿using DelightGenie.Models.DbModels.HistoryModels;

namespace DelightGenie.Models.ViewModels.AdminVM
{
    public class TransactionsVM
    {
        public Transaction[] Transactions { get; set; }
    }
}