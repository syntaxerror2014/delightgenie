﻿using System;
using DelightGenie.Models.DbModels.AccountModels;

namespace DelightGenie.Models.DbModels.NotificationModels
{
    public abstract class Notification : AbstractModel
    {
        public DateTime? NotificationDate { get; set; }

        public virtual Employee Employee { get; set; }
    }
}