﻿using System.Web.DynamicData;

namespace DelightGenie.Models.DbModels.NotificationModels
{
    [TableName("Notifications")]
    public class ProductCreateNotification : Notification
    {
        public string ProductName { get; set; }

        public string Description { get; set; }
    }
}