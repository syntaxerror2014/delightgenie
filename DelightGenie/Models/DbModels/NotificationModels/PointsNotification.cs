﻿using System.Web.DynamicData;

namespace DelightGenie.Models.DbModels.NotificationModels
{
    [TableName("PointsNotifications")]
    public class PointsNotification : Notification
    {
        public virtual Message Message { get; set; }
    }
}