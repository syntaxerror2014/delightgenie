﻿using System.Collections.ObjectModel;
using System.Web.DynamicData;

namespace DelightGenie.Models.DbModels.NotificationModels
{
    [TableName("Messages")]
    public class Message : AbstractModel
    {
        public const int IdentifierPointsDeleted = 0xA1;
        public const int IdentifierPointsAdded = 0xB2;
        public const int IdentifierTransactionsDeleted = 0x5F;

        public int MessageIdentifier { get; set; }

        public string MessageText { get; set; }

        public virtual Collection<PointsNotification> PointsNotifications { get; set; }
    }
}