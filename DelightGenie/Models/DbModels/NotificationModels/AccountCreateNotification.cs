﻿using System.Web.DynamicData;

namespace DelightGenie.Models.DbModels.NotificationModels
{
    [TableName("CreateAccountNotifications")]
    public class AccountCreateNotification : Notification
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }
    }
}