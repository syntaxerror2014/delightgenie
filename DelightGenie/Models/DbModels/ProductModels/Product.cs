﻿using System.Collections.ObjectModel;
using System.Web.DynamicData;
using DelightGenie.Models.DbModels.HistoryModels;

namespace DelightGenie.Models.DbModels.ProductModels
{
    [TableName("Products")]
    public class Product : AbstractModel
    {
        public string ProductName { get; set; }

        public string ProductDescription { get; set; }

        public float ProductPrice { get; set; }

        public virtual Category Category { get; set; }

        public virtual Collection<Transaction> Transactions { get; set; }

        public virtual Collection<Purchase> Purchases { get; set; }
    }
}