﻿using System.Collections.ObjectModel;
using System.Web.DynamicData;

namespace DelightGenie.Models.DbModels.ProductModels
{
    [TableName("Categories")]
    public class Category : AbstractModel
    {
        public string CategoryName { get; set; }

        public virtual Collection<Product> Products { get; set; }
    }
}