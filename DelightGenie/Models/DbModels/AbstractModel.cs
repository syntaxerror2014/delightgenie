﻿using System;

namespace DelightGenie.Models.DbModels
{
    public abstract class AbstractModel
    {
        public Guid Id { get; set; }
    }
}