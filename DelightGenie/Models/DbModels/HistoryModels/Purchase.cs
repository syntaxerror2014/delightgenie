﻿using System.Web.DynamicData;
using DelightGenie.Models.DbModels.ProductModels;

namespace DelightGenie.Models.DbModels.HistoryModels
{
    [TableName("Purchases")]
    public class Purchase : AbstractModel
    {
        public int NumberOfItems { get; set; }

        public float ItemPrice { get; set; }

        public int Points { get; set; }

        public virtual Commit Commit { get; set; }

        public virtual Product Product { get; set; }
    }
}