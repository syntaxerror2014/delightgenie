﻿using System;
using System.Collections.ObjectModel;
using System.Web.DynamicData;

namespace DelightGenie.Models.DbModels.HistoryModels
{
    [TableName("Commits")]
    public class Commit : AbstractModel
    {
        public DateTime? CommitDate { get; set; }

        public float Budget { get; set; }

        public virtual Collection<Purchase> Purchases { get; set; }
    }
}