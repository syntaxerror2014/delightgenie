﻿using System;
using System.Web.DynamicData;
using DelightGenie.Models.DbModels.AccountModels;
using DelightGenie.Models.DbModels.ProductModels;

namespace DelightGenie.Models.DbModels.HistoryModels
{
    [TableName("Transactions")]
    public class Transaction : AbstractModel
    {
        public int Points { get; set; }

        public DateTime? TransactionDate { get; set; }

        public virtual Product Product { get; set; }

        public virtual Employee Employee { get; set; }
    }
}