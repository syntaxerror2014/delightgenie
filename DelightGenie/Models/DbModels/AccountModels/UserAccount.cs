﻿using System.Web.DynamicData;

namespace DelightGenie.Models.DbModels.AccountModels
{
    [TableName("UserAccounts")]
    public abstract class UserAccount : AbstractModel
    {
        public string UserName { get; set; }

        public string Email { get; set; }

        public virtual Session Session { get; set; }
    }
}