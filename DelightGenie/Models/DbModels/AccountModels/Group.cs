﻿using System.Collections.ObjectModel;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations.Schema;

namespace DelightGenie.Models.DbModels.AccountModels
{
    [TableName("Groups")]
    public class Group : AbstractModel
    {
        public const string DefaultGroupName = "Default";

        public string GroupName { get; set; }

        public int Points { get; set; }

        public virtual Collection<Employee> Employees { get; set; }
    }
}