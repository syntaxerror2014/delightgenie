﻿using System.Web.DynamicData;

namespace DelightGenie.Models.DbModels.AccountModels
{
    [TableName("Admins")]
    public class Admin : UserAccount
    {
        public string Password { get; set; }
    }
}