﻿using System.Collections.ObjectModel;
using System.Web.DynamicData;
using DelightGenie.Models.DbModels.HistoryModels;
using DelightGenie.Models.DbModels.NotificationModels;

namespace DelightGenie.Models.DbModels.AccountModels
{
    [TableName("Employee")]
    public class Employee : UserAccount
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string WelcomeMessage { get; set; }

        public int Points { get; set; }

        public virtual Group Group { get; set; }

        public virtual Collection<Transaction> Transactions { get; set; }

        public virtual Collection<Notification> Notifications { get; set; }
    }
}