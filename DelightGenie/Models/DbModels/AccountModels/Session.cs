﻿using System.Web.DynamicData;

namespace DelightGenie.Models.DbModels.AccountModels
{
    [TableName("Session")]
    public class Session : AbstractModel
    {
        public string SessionKey { get; set; }

        public virtual UserAccount UserAccount { get; set; }
    }
}